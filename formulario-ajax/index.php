<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Formu</title>
	
	<!-- jquery -->
	<script  src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	
	<!-- bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script src="ckeditor/ckeditor.js"/>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
	<script src="js/sweetalert.js"></script>
	
</head>

<body>
	<div class="container">
		
		<br><br>
		<h3 id="textual-inputs">Email Marketing</h3>
		
		<br><br>
		
		<form action="" method="post" enctype="multipart/form-data" class="form">
			<div class="form-group row">
			  <label for="example-text-input" class="col-xs-2 col-form-label">Nombre*</label>
			  <div class="col-xs-10">
				<input class="form-control" name="nombre" type="text" value="" id="example-text-input" required>
			  </div>
			</div>
			<div class="form-group row">
			  <label for="example-search-input" class="col-xs-2 col-form-label">Email*</label>
			  <div class="col-xs-10">
				<input class="form-control" name="email" type="email" value="" id="example-search-input" >
			  </div>
			</div>
			<div class="form-group row">
			  <label for="example-email-input" class="col-xs-2 col-form-label">Teléfono</label>
			  <div class="col-xs-10">
				<input class="form-control" name="telefono" type="text" value="" id="example-email-input">
			  </div>
			</div>
			<div class="form-group row">
			  <label for="example-url-input" class="col-xs-2 col-form-label">Mensaje*</label>
			  <div class="col-xs-10">
				  <textarea rows="10" cols="80" class="form-control" name="mensaje" id="editor1" rows="10"></textarea>
			  </div>
			</div>

			<div class="form-group row">
			  <label class="col-xs-2 col-form-label"></label>
			  <div class="col-xs-10">
				  <button type="submit" class="btn btn-primary pull-left" id="btn-contato">Enviar</button>
				  <div class="loading" style="float: left;margin-left:20px;"></div>
			  </div>
			</div>
		</form>		
	
		<div class="mostrar"></div>
	</div><!-- container -->	

<script>
	$('.mensaje').ckeditor();
	jQuery.each(CKEDITOR.instances, function(){
     eval("CKEDITOR.instances."+this.name+".destroy(true)");
});
</script>
<script>
    CKEDITOR.replace( 'editor1' );
</script>

<script>
	$(function(){
		$('.form').submit(function(){
			$('.loading').html("<img src='loading.gif' width='45'>");
			$.ajax({
				url: 'send-form.php',
				type: 'POST',
				data: $('.form').serialize(),
				success: function(data){
					$('.mostrar').html(data);
					$('.loading').hide();
					$('.form')[0].reset();
				}
			});
			return false;
		});
	});
</script>	
	
	
	
	
	
</body>
</html>