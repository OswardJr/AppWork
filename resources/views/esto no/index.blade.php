@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            </head>

            <body>
                <div class="col-md-12">
                    <h2>Users</h2>
                    <br />
                        <a href="#" class="add-modal btnn btn btn-default"><i class="fa fa-plus"></i> Add a User</a>
                    <div class="panel panel-primary panel">
                        <div class="panel-heading">
                            <ul>
                                <li><i class="fa fa-file-text-o"></i> All the current Users</li>
                            </ul>
                        </div>
                    
                        <div class="panel-body">
                                <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                                    <thead>
                                        <tr>
                                            <th valign="middle">#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Actions</th>
                                        </tr>
                                        {{ csrf_field() }}
                                    </thead>
                                    <tbody>
                                        @foreach($usuarios as $indexKey => $usu)
                                            <tr class="item{{$usu->id}}">
                                                <td class="col1">{{ $indexKey+1 }}</td>
                                                <td>{{$usu->name}}</td>
                                                <td>
                                                    {{$usu->email}}
                                                </td>  
                                                                        
                                                <td>
                                                    <button class="show-modal btn btn-primary" data-id="{{$usu->id}}" data-name="{{$usu->name}}" data-email="{{$usu->email}}">
                                                    <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                                    <button class="edit-modal btn btn-warning" data-id="{{$usu->id}}" data-name="{{$usu->name}}" data-email="{{$usu->email}}">
                                                    <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                                    <button class="delete-modal btn btn-danger" data-id="{{$usu->id}}" data-name="{{$usu->name}}" data-email="{{$usu->email}}">
                                                    <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel panel-default -->
                </div><!-- /.col-md-8 -->

                <!-- Modal form to add a user -->
                <div id="addModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" role="form" id="form-insert">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Name:</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="name_add" placeholder="Osward Pacheco" value="{{ old('name') }}" required>
                                            <p class="errorName text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="email">Email:</label>
                                        <div class="col-md-10">
                                            <input type="email" class="form-control" id="email_add" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                            <p class="errorEmail text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="password">Password:</label>
                                        <div class="col-md-10">
                                            <input type="password" min="6" class="form-control" id="password_add" placeholder="xxxxxxxx" value="{{ old('password') }}" required>
                                            <p class="errorPassword text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                         
                                    <div class="col-md-10 col-md-offset-2">
                                      <h3>Permisos</h3>
                                      <p>Seleccione las funciones a las que tendrá acceso el usuario</p>            
                                      <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Modulo / Permiso</th>
                                            <th>Habilitar</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($modules as $mod)
                                          <tr>
                                            <td>{{ ($mod->nombre) }}</td>
                                            <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" 
                                                           value="{{ ($mod->id) }}" 
                                                           name="modules[]"  
                                                           id="modules" 
                                                    ></label>
                                            </div>
            
                                            </td>
                                          </tr>
                                        @endforeach
                                        </tbody>
                                      </table>
                                    </div>

                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary add" data-dismiss="modal">
                                        <span id="" class='glyphicon glyphicon-check'></span> Add
                                    </button>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        <span class='glyphicon glyphicon-remove'></span> Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Modal form to show a post -->
                <div id="showModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Name:</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="show_name" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="email">Email:</label>
                                        <div class="col-md-10">
                                            <input type="name" class="form-control" id="email" disabled>
                                        </div>
                                    </div>                        
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        <span class='glyphicon glyphicon-remove'></span> Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Modal form to edit a form -->
                <div id="editModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="id">ID:</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="id_edit" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Name:</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="name_edit" placeholder="Osward Pacheco" value="{{ old('name') }}" required>
                                            <p class="errorName text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="email">Email:</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="email_edit" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                            <p class="errorEmail text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="password">Password:</label>
                                        <div class="col-md-10">
                                            <input type="password" class="form-control" id="password_edit" placeholder="xxxxxx" value="{{ old('password') }}" required>
                                            <p class="errorPassword text-center alert alert-danger hidden"></p>
                                        </div>
                                    </div>
                                                                        
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                                        <span class='glyphicon glyphicon-check'></span> Edit
                                    </button>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        <span class='glyphicon glyphicon-remove'></span> Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal form to delete a form -->
                <div id="deleteModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <h3 class="text-center">¿you want to delete this user?</h3>
                                <br />
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="id">ID:</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="id_delete" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2" for="name">Name:</label>
                                        <div class="col-md-10">
                                            <input type="name" class="form-control" id="name_delete" disabled>
                                        </div>
                                    </div>
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                        <span id="" class='glyphicon glyphicon-trash'></span> Delete
                                    </button>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        <span class='glyphicon glyphicon-remove'></span> Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- jQuery -->
                <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

                <script>
                    $(window).load(function(){
                        $('#mytable').removeAttr('style');
                    })
                </script>

                <!-- AJAX CRUD operations -->
                <script type="text/javascript">
                    // add a new post
                    $(document).on('click', '.add-modal', function() {
                        $('.modal-title').text('Add');
                        $('#addModal').modal('show');
                    });
                    $('.modal-footer').on('click', '.add', function() {
                        $.ajax({
                            type: 'POST',
                            url: 'users',
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'name': $('#name_add').val(),
                                'email': $('#email_add').val(),
                                'password':$('#password_add').val(),
                                'modules':$('#modules').val()
                            },
                            success: function(data) {
                                $('.errorName').addClass('hidden');
                                $('.errorEmail').addClass('hidden');
                                $('.errorPassword').addClass('hidden');

                                if ((data.errors)) {
                                    setTimeout(function () {
                                        $('#addModal').modal('show');
                                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                    }, 500);

                                    if (data.errors.d) {
                                        $('.errorName').removeClass('hidden');
                                        $('.errorName').text(data.errors.d);
                                    }
                                    if (data.errors.email) {
                                        $('.errorEmail').removeClass('hidden');
                                        $('.errorEmail').text(data.errors.email);
                                    } 
                                    if (data.errors.password) {
                                        $('.errorPassword').removeClass('hidden');
                                        $('.errorPassword').text(data.errors.password);
                                    }                                                                         
                                } else {
                                    toastr.success('Successfully added User!', 'Success Alert', {timeOut: 5000});

                                    $('#form-insert')[0].reset();
                                    
                                    $('#mytable').prepend("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "' data-password='" + data.password + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "' data-password='" + data.password + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "' data-password='" + data.password + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                                    $('.col1').each(function (index) {
                                        $(this).html(index+1);
                                    });
                                }
                            },
                        });
                    });

                    // Show a user
                    $(document).on('click', '.show-modal', function() {
                        $('.modal-title').text('Show');
                        $('#show_name').val($(this).data('name'));
                        $('#email').val($(this).data('email'));
                        $('#showModal').modal('show');
                    });


                    // Edit a user
                    $(document).on('click', '.edit-modal', function() {
                        $('.modal-title').text('Edit');
                        $('#id_edit').val($(this).data('id'));
                        $('#name_edit').val($(this).data('name'));
                        $('#email_edit').val($(this).data('email'));
                        $('#password_edit').val($(this).data('password'));
                        $('#rol_edit').val($(this).data('rol'));
                        id = $('#id_edit').val();
                        $('#editModal').modal('show');
                    });
                    $('.modal-footer').on('click', '.edit', function() {
                        $.ajax({
                            type: 'PUT',
                            url: 'users/' + id,
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'id': $("#id_edit").val(),
                                'name': $('#name_edit').val(),
                                'email': $('#email_edit').val(),
                                'password': $('#password_edit').val(),
                                'rol': $('#rol_edit').val()
                            },
                            success: function(data) {
                                $('.errorName').addClass('hidden');
                                $('.errorEmail').addClass('hidden');
                                $('.errorPassword').addClass('hidden');
                                $('.errorRol').addClass('hidden');

                                if ((data.errors)) {
                                    setTimeout(function () {
                                        $('#editModal').modal('show');
                                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                    }, 500);

                                    if (data.errors.name) {
                                        $('.errorName').removeClass('hidden');
                                        $('.errorName').text(data.errors.name);
                                    }
                                    if (data.errors.email) {
                                        $('.errorEmail').removeClass('hidden');
                                        $('.errorEmail').text(data.errors.email);
                                    }
                                    if (data.errors.password) {
                                        $('.errorPassword').removeClass('hidden');
                                        $('.errorPassword').text(data.errors.password);
                                    } 
                                    if (data.errors.rol) {
                                        $('.errorRol').removeClass('hidden');
                                        $('.errorRol').text(data.errors.rol);
                                    }                                                
                                } else {
                                    toastr.success('Successfully updated User!', 'Success Alert', {timeOut: 5000});
                                    $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "' data-password='" + data.password + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "' data-password='" + data.password + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' data-email='" + data.email + "' data-password='" + data.password + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");

                                    $('.col1').each(function (index) {
                                        $(this).html(index+1);
                                    });
                                }
                            }
                        });
                    });
                    
                    // delete a user
                    $(document).on('click', '.delete-modal', function() {
                        $('.modal-title').text('Delete');
                        $('#id_delete').val($(this).data('id'));
                        $('#name_delete').val($(this).data('name'));
                        $('#deleteModal').modal('show');
                        id = $('#id_delete').val();
                    });
                    $('.modal-footer').on('click', '.delete', function() {
                        $.ajax({
                            type: 'DELETE',
                            url: 'users/' + id,
                            data: {
                                '_token': $('input[name=_token]').val(),
                            },
                            success: function(data) {
                                toastr.success('Successfully deleted User!', 'Success Alert', {timeOut: 5000});
                                $('.item' + data['id']).remove();
                                $('.col1').each(function (index) {
                                    $(this).html(index+1);
                                });
                            }
                        });
                    });
                </script>

            </body>
        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')