@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

</head>

<body>
    <div class="col-md-12">
        <h2>New Contact</h2>
        <br />
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i>New Contact</li>
                </ul>
            </div>
        
            <div class="panel-body">
                    <form action="{{route('contacts.store')}}" role="form" id="form-insert" method="POST">
                        {{csrf_field()}}
                        <div class="form-group col-sm-6">
                            <label for="company_id">Company:</label>
                           <select name="company_id" id="company_id" class="form-control select2">
                               @foreach($companies as $c)
                                    <option value="{{$c->id}}">{{$c->name3}}</option>
                               @endforeach
                           </select>
                        </div>

                         <div class="form-group col-sm-6">
                            <label for="company_id">First Name:</label>
                           <input type="text" name="first" class="form-control" placeholder="Name">
                        </div>

                            <div class="form-group col-sm-6">
                            <label for="company_id">Phone:</label>
                           <input type="text" name="phone" class="form-control" placeholder="Phone">
                        </div>

                         <div class="form-group col-sm-6">
                            <label for="company_id">Email:</label>
                           <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        
                        <div class="form-group text-center">
                          <button class="btn btn-primary" type="submit">Guardar</button>
                           <a href="{{route('companies.index')}}" class="btn btn-danger" type="submit">Regresar</a>
                        </div>
                   </form>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->







    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

@include('layouts.footer')