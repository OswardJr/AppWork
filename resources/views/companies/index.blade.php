@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height:1400px">

</head>

<body>
    <div class="col-md-12">
        <h2>Company</h2>
        <br />
        @if(session('success'))
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="alert alert-success">{{session('success')}}</div>
        </div>
        @endif
            <a href="contacts" class="btn btn-default"><i class="fa fa-plus"></i> List Contacts</a>
            <a href="#" class="add-modal btnn btn btn-default"><i class="fa fa-plus"></i> Add a Company</a>
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Company</li>
                </ul>
            </div>
        
            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th valign="middle">#</th>
                                <th>Identification</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Type</th>
                                <th>Contacts</th>
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($companies as $indexKey => $company)
                                <tr class="item{{$company->id}}">
                                    <td class="col1">{{ $indexKey+1 }}</td>

                                    <td>{{$company->identification}}</td>
                                    <td>
                                        {{$company->name3}}
                                    </td>
                                    <td>
                                        {{$company->phone}}
                                    </td>                                    
                                    <td>
                                        {{$company->address}}
                                    </td>                                  
                                    <td>
                                        {{$company->type}}
                                    </td>
                                    <td>
                                        {{$company->contacts->pluck('first')->implode(', ')}}

                                    </td>
                                    <td>
                                        <button class="show-modal btn btn-primary" data-id="{{$company->id}}" data-identification="{{$company->identification}}"  data-name3="{{$company->name3}}" data-country="{{$company->country}}" data-address="{{$company->address}}" data-phone="{{$company->phone}}" data-email="{{$company->email}}" data-url="{{$company->url}}" data-type="{{$company->type}}" data-contacts="{{$company->contacts->pluck('first')->implode(', ')}}">
                                        <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                        <button class="edit-modal btn btn-warning" data-id="{{$company->id}}" data-identification="{{$company->identification}}"  data-name3="{{$company->name3}}" data-country="{{$company->country}}" data-address="{{$company->address}}" data-phone="{{$company->phone}}" data-email="{{$company->email}}" data-url="{{$company->url}}" data-type="{{$company->type}}" data-contacts="{{$company->contacts->pluck('first')->implode(', ')}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        <button class="delete-modal btn btn-danger" data-id="{{$company->id}}" data-identification="{{$company->identification}}"  data-name3="{{$company->name3}}" data-country="{{$company->country}}" data-address="{{$company->address}}" data-phone="{{$company->phone}}" data-email="{{$company->email}}" data-url="{{$company->url}}" data-type="{{$company->type}}" data-contacts="{{$company->contacts->pluck('first')->implode(', ')}}">
                                        <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        <a class="btn btn-default" href="{{route('companies.show',$company->id)}}">
                                        <span class="glyphicon glyphicon-plus"></span> Add Contact</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

    <!-- Modal form to add a company -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" id="form-insert">

                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">Identification:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="identification" placeholder="J-32323-23" value="{{ old('identification') }}" required>
                                <p class="errorIden text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">Name:</label>
                            <div class="col-md-10">
                                <input type="text" id="name" class="form-control" placeholder="Donna Novedades C.A" value="{{ old('name3') }}" required>
                                <p class="errorNames text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="countries" class="control-label col-md-2">Country:</label>
                            <div class="col-md-10">
                            <select id="country" class="select2 form-control"  style="width: 100%;" required>
                                <option value="">Select Country</option>
                                 @foreach($countries as $country)
                                <option value="{{$country->Pais}}">{{$country->Pais}}</option>
                                @endforeach
                            </select>
                                <p class="errorCountry text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="control-label col-md-2">Direction:</label>
                            <div class="col-md-10">
                                <textarea id="address" cols="30" rows="2" class="form-control"></textarea>
                                <p class="errorAddress text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" id="phone" placeholder="1351923289" value="{{ old('phone') }}" required>
                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        
                        

                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Email:</label>
                            <div class="col-md-10">
                                <input type="email" class="form-control" id="email" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>   

                        <div class="form-group">
                            <label class="control-label col-md-2" for="url">URL:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="Url" placeholder="www.donnanovedades.com"  value="{{ old('url') }}" required>
                                <p class="errorUrl text-center alert alert-danger hidden"></p>
                            </div>
                        </div>   
                        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="type">Type:</label>
                            <div class="col-md-10">
                                <select id="type" class="form-control" required>
                                    <option value="">Select person type</option>
                                    <option value="Client">Client</option>
                                    <option value="Provider">Provider</option>
                                </select>
                                <p class="errorType text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                      
                   </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to show a post -->
    <div id="showModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                       <form class="form-horizontal" role="form" id="form-insert">

                        <div class="form-group">
                            <label class="control-label col-md-2" for="show_identification">Identification:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="show_identification" placeholder="J-32323-23" value="{{ old('identification') }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="show_name">Name:</label>
                            <div class="col-md-10">
                                <input type="text" id="show_name" class="form-control" placeholder="Donna Novedades C.A" value="{{ old('identification') }}" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="show_country" class="control-label col-md-2">Country:</label>
                            <div class="col-md-10">
                            <input id="show_country" class="form-control" disabled  style="width: 100%;">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="show_address" class="control-label col-md-2">Direction:</label>
                            <div class="col-md-10">
                                <textarea id="show_address" cols="30" rows="2" class="form-control" disabled></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="show_phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" id="show_phone" placeholder="1351923289" value="{{ old('phone') }}" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="show_email">Email:</label>
                            <div class="col-md-10">
                                <input type="email" class="form-control" id="show_email" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" disabled>
                            </div>
                        </div>   

                        <div class="form-group">
                            <label class="control-label col-md-2" for="show_url">URL:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="show_url" placeholder="www.donnanovedades.com"  value="{{ old('url') }}" disabled>
                            </div>
                        </div>   
                        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="show_type">Type:</label>
                            <div class="col-md-10">
                                <input  id="show_type" class="form-control" disabled>
                            </div>
                        </div>                         
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>


    <!-- Modal form to edit a form -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id"></label>
                                <div class="col-md-10">
                                    <input type="hidden" class="form-control" id="id_edit" disabled>
                            </div>
                        </div>                         
                        <div class="form-group">
                            <label class="control-label col-md-2" for="identification">Identification:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="edit_identification" placeholder="J-32323-23" value="{{ old('identification') }}" required>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="name">Name:</label>
                            <div class="col-md-10">
                                <input type="text" id="edit_name" class="form-control" placeholder="Donna Novedades C.A" value="{{ old('identification') }}" required>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="countries" class="control-label col-md-2">Country:</label>
                            <div class="col-md-10">
                            <select id="edit_country" class="select2 form-control"  style="width: 100%;">
                                <option value="">Select Country</option>
                                 @foreach($countries as $country)
                                <option value="{{$country->Pais}}">{{$country->Pais}}</option>
                                @endforeach
                            </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="control-label col-md-2">Direction:</label>
                            <div class="col-md-10">
                                <textarea id="edit_address" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" id="edit_phone" placeholder="1351923289" value="{{ old('phone') }}" required>
                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        
                        

                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Email:</label>
                            <div class="col-md-10">
                                <input type="email" class="form-control" id="edit_email" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>   

                        <div class="form-group">
                            <label class="control-label col-md-2" for="url">URL:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="edit_url" placeholder="www.donnanovedades.com"  value="{{ old('url') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>   
                        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="type">Type:</label>
                            <div class="col-md-10">
                                <select id="edit_type" class="form-control">
                                    <option value="">Select person type</option>
                                    <option value="Client">Client</option>
                                    <option value="Provider">Provider</option>
                                </select>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">¿you want to delete this contact?</h3>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="title">First Name:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="first_delete" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <script>
        $(window).load(function(){
            $('#mytable').removeAttr('style');
        })
    </script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
                    // add a new company
                    $(document).on('click', '.add-modal', function() {
                        $('.modal-title').text('Add');
                        $('#addModal').modal('show');
                    });
                    $('.modal-footer').on('click', '.add', function() {
                        var identification,name,country,address,phone,email,Url,contacts;
                        
                        $.ajax({
                            type: 'POST',
                            url: 'companies',
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'identification': $('#identification').val(),
                                'name3': $('#name').val(),
                                'country': $('#country').val(),
                                'address':$('#address').val(),
                                'phone': $('#phone').val(),
                                'email': $('#email').val(),
                                'url': $('#Url').val(),
                                'type':$('#type').val(),
                                'contacts':$('#contacts').val()
                            },
                            success: function(data) {
                                $('.errorIden').addClass('hidden');
                                $('.errorNames').addClass('hidden');
                                $('.errorCountry').addClass('hidden');
                                $('.errorAddress').addClass('hidden');
                                $('.errorPhone').addClass('hidden');
                                $('.errorEmail').addClass('hidden');
                                $('.errorUrl').addClass('hidden');
                                $('.errorType').addClass('hidden');
                                $('.errorContacts').addClass('hidden');

                                // if ((data.errors)) {
                                //     setTimeout(function () {
                                //         $('#addModal').modal('show');
                                //         toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                //     }, 500);

                                //     if (data.errors.identification) {
                                //         $('.errorIden').removeClass('hidden');
                                //         $('.errorIden').text(data.errors.identification);
                                //     }
                                //     if (data.errors.name) {
                                //         $('.errorNames').removeClass('hidden');
                                //         $('.errorNames').text(data.errors.name);
                                //     }
                                //     if (data.errors.country) {
                                //         $('.errorCountry').removeClass('hidden');
                                //         $('.errorCountry').text(data.errors.country);
                                //     }
                                //     if (data.errors.address) {
                                //         $('.errorAddress').removeClass('hidden');
                                //         $('.errorAddress').text(data.errors.address);
                                //     }
                                //     if (data.errors.phone) {
                                //         $('.errorPhone').removeClass('hidden');
                                //         $('.errorPhone').text(data.errors.phone);
                                //     }                                        
                                //     if (data.errors.email) {
                                //         $('.errorEmail').removeClass('hidden');
                                //         $('.errorEmail').text(data.errors.email);
                                //     }                                                 
                                // } else {                             
                            toastr.success('Successfully added Company!', 'Success Alert', {timeOut: 5000});

                             $('#addModal').modal('hide');

                              $('#form-insert')[0].reset();
                              $("#country").select2('destroy'); 
                              $("#country").html("<option></option>"); 
                              $("#country").select2();
                              $("#contacts").select2('destroy'); 
                              $("#contacts").html("<option></option>"); 
                              $("#contacts").select2();
                            
                            $('#mytable').prepend("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.identification + "</td><td>" + data.name3 + "</td><td>" + data.phone + "</td><td>" + data.address + "</td><td>"+ data.type +"</td><td>"+ data.contacts +"</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-identification='" + data.identification + "' data-name3='" + data.name3 + "' data-country='" + data.country + "' data-address='" + data.address + "' data-phone='"+ data.phone +"' data-email='"+ data.email +"' data-url='"+ data.url +"' data-type='"+ data.type +"' data-contacts='"+ data.contacts +"''><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-identification='" + data.identification + "' data-name3='" + data.name3 + "' data-country='" + data.country + "' data-address='" + data.address + "' data-phone='"+ data.phone +"' data-email='"+ data.email +"' data-url='"+ data.url +"' data-type='"+ data.type +"' data-contacts='"+ data.contacts +"'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-identification='" + data.identification + "' data-name3='" + data.name3 + "' data-country='" + data.country + "' data-address='" + data.address + "' data-phone='"+ data.phone +"' data-email='"+ data.email +"' data-url='"+ data.url +"' data-type='"+ data.type +"' data-contacts='"+ data.contacts +"'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                                    $('.col1').each(function (index) {
                                        $(this).html(index+1);
                                    });

                            }
                        });
                    });

        // Show a company
                    $(document).on('click', '.show-modal', function() {
                        $('.modal-title').text('Show');
                        $('#show_identification').val($(this).data('identification'));
                        $('#show_name').val($(this).data('name3'));
                        $('#show_country').val($(this).data('country'));
                        $('#show_address').val($(this).data('address'));
                        $('#show_phone').val($(this).data('phone'));
                        $('#show_email').val($(this).data('email'));
                        $('#show_url').val($(this).data('url'));
                        $('#show_type').val($(this).data('type'));
                        $('#showModal').modal('show');
                    });


                    // Edit a company
                    $(document).on('click', '.edit-modal', function() {
                        $('.modal-title').text('Edit');
                        $('#id_edit').val($(this).data('id'));
                        $('#edit_identification').val($(this).data('identification'));
                        $('#edit_name').val($(this).data('name3'));
                        $('#edit_country').val($(this).data('country'));
                        $('#edit_address').val($(this).data('address'));
                        $('#edit_phone').val($(this).data('phone'));
                        $('#edit_email').val($(this).data('email'));
                        $('#edit_url').val($(this).data('url'));
                        $('#edit_type').val($(this).data('type'));

                        id = $('#id_edit').val();
                        $('#editModal').modal('show');
                    });
                    $('.modal-footer').on('click', '.edit', function() {
                        $.ajax({
                            type: 'PUT',
                            url: 'companies/' + id,
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'id': $("#id_edit").val(),
                                'identification': $('#edit_identification').val(),
                                'name3': $('#edit_name').val(),
                                'country': $('#edit_country').val(),
                                'address': $('#edit_address').val(),
                                'phone': $('#edit_phone').val(),
                                'email': $('#edit_email').val(),
                                'url': $('#edit_url').val(),
                                'type': $('#edit_type').val()
                            },
                            success: function(data) {
                                $('.errorName').addClass('hidden');
                                $('.errorEmail').addClass('hidden');
                                $('.errorPassword').addClass('hidden');
                                $('.errorRol').addClass('hidden');

                                if ((data.errors)) {
                                    setTimeout(function () {
                                        $('#editModal').modal('show');
                                        toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                    }, 500);

                                    if (data.errors.name) {
                                        $('.errorName').removeClass('hidden');
                                        $('.errorName').text(data.errors.name);
                                    }
                                    if (data.errors.email) {
                                        $('.errorEmail').removeClass('hidden');
                                        $('.errorEmail').text(data.errors.email);
                                    }
                                    if (data.errors.password) {
                                        $('.errorPassword').removeClass('hidden');
                                        $('.errorPassword').text(data.errors.password);
                                    } 
                                    if (data.errors.rol) {
                                        $('.errorRol').removeClass('hidden');
                                        $('.errorRol').text(data.errors.rol);
                                    }                                                
                                } else {
                                    toastr.success('Successfully updated Company!', 'Success Alert', {timeOut: 5000});
                            $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.identification + "</td><td>" + data.name3 + "</td><td>" + data.phone + "</td><td>" + data.address + "</td><td>"+ data.type +"</td><td>"+ data.contacts +"</td><td><button class='show-modal btn btn-primary' data-identification='" + data.identification + "' data-name3='" + data.name3 + "' data-country='" + data.country + "' data-address='" + data.address + "' data-phone='"+ data.phone +"' data-email='"+ data.email +"' data-url='"+ data.url +"' data-type='"+ data.type +"' data-contacts='"+ data.contacts +"''><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-identification='" + data.identification + "' data-name3='" + data.name3 + "' data-country='" + data.country + "' data-address='" + data.address + "' data-phone='"+ data.phone +"' data-email='"+ data.email +"' data-url='"+ data.url +"' data-type='"+ data.type +"' data-contacts='"+ data.contacts +"'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-identification='" + data.identification + "' data-name3='" + data.name3 + "' data-country='" + data.country + "' data-address='" + data.address + "' data-phone='"+ data.phone +"' data-email='"+ data.email +"' data-url='"+ data.url +"' data-type='"+ data.type +"' data-contacts='"+ data.contacts +"'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                                    $('.col1').each(function (index) {
                                        $(this).html(index+1);
                                    });
                                }
                            }
                        });
                    });
        
        // delete a company
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));
            $('#first_delete').val($(this).data('type'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: 'companies/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    toastr.success('Successfully deleted Company!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data['id']).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>

</body>

        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')