@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height:1000px">

</head>

<body>
    <div class="col-md-12">
        <h2>Alls Companys</h2>
        <br />
            <a href="{{ route('pdfcompany',['download'=>'pdf']) }}" class="add-modal btnn btn btn-default" target="_blank"><i class="fa fa-plus"></i> PDF</a>
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Companys</li>
                </ul>
            </div>
        
            <div class="panel-body">
                    <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th valign="middle">#</th>
                                <th>Identification</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Type</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($reports as $indexKey => $contact)
                                <tr class="item{{$contact->id}}">
                                    <td class="col1">{{ $indexKey+1 }}</td>
                                    <td>{{$contact->identification}}</td>
                                    <td>
                                        {{$contact->name3}}
                                    </td>
                                    <td>
                                        {{$contact->phone}}
                                    </td> 
                                    <td>
                                        {{$contact->email}}
                                    </td>
                                    <td>
                                        {{$contact->type}}
                                    </td>                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

    <!-- Modal form to add a post -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="form-insert" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">First Name:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="first_add" placeholder="Osward Pacheco" value="{{ old('first') }}" required>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" id="phone_add" placeholder="1351923289" value="{{ old('phone') }}" required>
                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Email:</label>
                            <div class="col-md-10">
                                <input type="email" class="form-control" id="email_add" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')