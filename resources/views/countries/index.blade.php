@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height:1000px">

</head>

<body>
    <div class="col-md-12">
        <h2>Countries</h2>
        <br />
            <a href="#" class="add-modal btnn btn btn-default"><i class="fa fa-plus"></i> Add a Country</a>
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Countrys</li>
                </ul>
            </div>
        
            <div class="panel-body">
                    <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th valign="middle">#</th>
                                <th>Code</th>
                                <th>Country</th>
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($countries as $indexKey => $count)
                                <tr class="item{{$count->id}} ">
                                    <td class="col1">{{ $indexKey+1 }}</td>
                                    <td>{{$count->Codigo}}</td>
                                    <td>
                                        {{$count->Pais}}
                                    </td>                                   
                                    <td>
                                        <button class="show-modal btn btn-primary" data-id="{{$count->id}}" data-code="{{$count->Codigo}}" data-country="{{$count->Pais}}">
                                        <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                        <button class="edit-modal btn btn-warning" data-id="{{$count->id}}" data-code="{{$count->Codigo}}" data-country="{{$count->Pais}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        <button class="delete-modal btn btn-danger" data-id="{{$count->id}}" data-code="{{$count->Codigo}}" data-country="{{$count->Pais}}">
                                        <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

    <!-- Modal form to add a post -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="form-insert" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="Codigo">Code:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="codigo_add" placeholder="EX" value="{{ old('first') }}" required>
                                <p class="errorCode text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="Pais">Country:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="pais_add" placeholder="Example" value="{{ old('phone') }}" required>
                                <p class="errorCountry text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to show a post -->
    <div id="showModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="Codigo">Code:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="show_codigo" placeholder="EX" required disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="Pais">Country:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Example" id="show_pais" required disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to edit a form -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_edit" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="code">Code:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="first_edit" placeholder="WS" value="{{ old('first') }}" required>
                                <p class="errorCode text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="country">Country:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="country_edit" value="{{ old('phone') }}" required>
                                <p class="errorCountry text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">¿you want to delete this Country?</h3>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="Pais">Country:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="delete_Pais" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')

 <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        // add a new post
        $(document).on('click', '.add-modal', function() {
            $('.modal-title').text('Add');
            $('#addModal').modal('show');
        });
        $('.modal-footer').on('click', '.add', function() {
            $.ajax({
                type: 'POST',
                url: 'countries',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'Codigo': $('#codigo_add').val(),
                    'Pais': $('#pais_add').val()
                },
                success: function(data) {
                    $('.errorCode').addClass('hidden');
                    $('.errorCountry').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#addModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.code) {
                            $('.errorCode').removeClass('hidden');
                            $('.errorCode').text(data.errors.code);
                        }
                        if (data.errors.country) {
                            $('.errorCountry').removeClass('hidden');
                            $('.errorCountry').text(data.errors.country);
                        }                         
                    } else {
                        toastr.success('Successfully added Country!', 'Success Alert', {timeOut: 5000});

                        $('#form-insert')[0].reset();

                        $('#mytable').prepend("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.Codigo + "</td><td>" + data.Pais + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-code='" + data.Codigo + "' data-country='" + data.Pais + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-code='" + data.Codigo + "' data-country='" + data.Pais + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-code='" + data.Codigo + "' data-country='" + data.Pais + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                        $('.col1').each(function (index) {
                            $(this).html(index+1);
                        });
                    }
                },
            });
        });

        // Show a post
        $(document).on('click', '.show-modal', function() {
            $('.modal-title').text('Show');
            $('#show_codigo').val($(this).data('code'));
            $('#show_pais').val($(this).data('country'));
            $('#showModal').modal('show');
        });


        // Edit a post
        $(document).on('click', '.edit-modal', function() {
            $('.modal-title').text('Edit');
            $('#id_edit').val($(this).data('id'));
            $('#first_edit').val($(this).data('code'));
            $('#country_edit').val($(this).data('country'));
            id = $('#id_edit').val();
            $('#editModal').modal('show');
        });
        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'PUT',
                url: 'countries/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#id_edit").val(),
                    'Codigo': $('#first_edit').val(),
                    'Pais': $('#country_edit').val()
                },
                success: function(data) {
                    $('.errorCode').addClass('hidden');
                    $('.errorCountry').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#editModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.code) {
                            $('.errorCode').removeClass('hidden');
                            $('.errorCode').text(data.errors.code);
                        }
                        if (data.errors.country) {
                            $('.errorCountry').removeClass('hidden');
                            $('.errorCountry').text(data.errors.country);
                        }                        
                    } else {
                        toastr.success('Successfully updated Country!', 'Success Alert', {timeOut: 5000});
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.Codigo + "</td><td>" + data.Pais + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-code='" + data.Codigo + "' data-country='" + data.Pais + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-code='" + data.Codigo + "' data-country='" + data.Pais + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-code='" + data.Codigo + "' data-country='" + data.Pais + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");

                        $('.col1').each(function (index) {
                            $(this).html(index+1);
                        });
                    }
                }
            });
        });
        
        // delete a post
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));
            $('#delete_Pais').val($(this).data('country'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: 'countries/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    toastr.success('Successfully deleted Country!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data['id']).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>
