@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper" style="height: auto;">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height: 1000px;">

</head>

<body>
    <div class="col-md-12">
        <h2>Invoice</h2>
        <br />
         @if(session('success'))
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="alert alert-success">{{session('success')}}</div>
        </div>
        @endif
        <div class="col-md-12" style="display: flex;justify-content: flex-end;">
            <a href="{{route('invoice.create')}}" class="btn btn-default"><i class="fa fa-plus"></i> Add a Invoice</a>
        </div>
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Invoice</li>
                </ul>
            </div>
        
            <div class="panel-body">
                <div class="responsive">
                    <table class="table table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th>Invoice Number</th>
                                <th>Company</th>
                                <th>State</th>
                                <th>Rode</th>
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($invoices as $invoi)
                                <tr class="item{{$invoi->id}}">
                                    <td>
                                    #{{$invoi->invoice_number}}
                                    </td>
                                    <td>
                                        {{$invoi->name3}}
                                    </td>
                                    <td>
                                        {{$invoi->name2}}
                                    </td>                         
                                    <td>
                                        {{$invoi->rode}}
                                    </td>
                                    <td>
                                      <button class="show-modal btn btn-primary" data-id="{{$invoi->id}}" data-invoice_number="{{$invoi->invoice_number}}" data-company="{{$invoi->name3}}" data-di="{{$invoi->date_issue}}" data-de="{{$invoi->date_expiration}}" data-rode="{{$invoi->rode}}" data-state="{{$invoi->name2}}" data-file="{{$invoi->file}}">
                                        <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                        <a class="btn btn-warning" href="{{route('invoice.edit',$invoi->id)}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        <a class="btn btn-danger" href="{{route('invoiceBorrar',$invoi->id)}}" onclick="return confirm('Seguro desea eliminar esta factura la factura?')">
                                        <span class="glyphicon glyphicon-trash"></span> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->


    <!-- Modal form to show a post -->
    <div id="showModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label">Company / Client</label>
                            <input type="text" id="show_company" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>N° Invoice</label>
                            <input type="text" id="show_invoice_number" class="form-control" disabled>
                        </div>                    
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">Date of issue</label>
                        <input type="date" id="show_date_issue" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                    <label for="">Date Expiration</label>
                    <input type="date" id="show_date_expiration" class="form-control" disabled>
                    </div> 
            </div>

             <div class="row">
                    <div class="form-group col-sm-6">
                    <label for="">State</label>
                    <input type="text" id="show_state" class="form-control" disabled>
                    </div>
                    <div class="form-group col-sm-6">
                    <label for="">Rode</label>
                    <input type="text" id="show_rode" class="form-control" disabled>
                    </div>
                </div>

              <a href="#" onclick="" id="file" ></a>

             <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
            </div>
        </div>
    </div>
</div>


<div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="control-label">ID</label>
                            <input type="text" id="delete_id" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>N° Invoice</label>
                            <input type="text" id="delete_invoice_number" class="form-control" disabled>
                        </div>                    
                </div>


             <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                    <span id="" class='glyphicon glyphicon-trash'></span> Delete
                                </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
            </div>
        </div>
    </div>
</div>


    <!-- Modal form to edit a form -->
      

@include('layouts.footer')
<!-- AJAX CRUD operations -->
    <script type="text/javascript">
                    
        // Show a company
                    $(document).on('click', '.show-modal', function() {
                        $('.modal-title').text('Show');
                        $('#show_company').val($(this).data('company'));
                        $('#show_invoice_number').val($(this).data('invoice_number'));
                        $('#show_date_issue').val($(this).data('di'));
                        $('#show_date_expiration').val($(this).data('de'));
                        $('#show_rode').val($(this).data('rode'));
                        $('#show_state').val($(this).data('state'));
                        $('#file').attr("onclick","archivo('"+$(this).data('file')+"')");
                        $('#file').html($(this).data('file'));
                        $('#showModal').modal('show');
                    });
        
    </script>

    <script>
        function archivo(archivo){
                window.open(archivo,"width=200","height=100");
        }
    </script>

</body>

        </div>  
