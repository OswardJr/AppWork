@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

</head>

<body>
    <div class="col-md-12">
        <h2> Edit Invoice</h2>
        <br />
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Invoice</li>
                </ul>
            </div>
        
            <div class="panel-body">
              <form action="{{route('invoice.update',$invoice->id)}}" id="form-insert" enctype="multipart/form-data" method="POST"> 
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Company / Client</label>
                            <select id="company_id" name="company_id" class="select2 form-control"  style="width: 100%;">
                                <option value="">Select Company/Client</option>
                                 @foreach($companies as $c)
                                <option value="{{$c->id}}" {{$invoice->company_id ? 'selected' : ''}}>{{$c->name3}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('company_id'))
                            <span class="text-danger">
                                <strong>{{$errors->first('company_id')}}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group col-sm-6">
                            <label>N° Invoice</label>
                            <input type="text" id="invoice_number" name="invoice_number" class="form-control" placeholder="#2322334-23" value="{{$invoice->invoice_number}}">
                            @if($errors->has('invoice_number'))
                            <span class="text-danger">
                                <strong>{{$errors->first('invoice_number')}}</strong>
                            </span>
                            @endif
                        </div>                    
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">Date of issue</label>
                        <input type="date" id="date_issue" name="date_issue" class="form-control" value="{{$now->format('Y-m-d')}}" value="{{$invoice->date_issue}}">
                       @if($errors->has('date_issue'))
                            <span class="text-danger">
                                <strong>{{$errors->first('date_issue')}}</strong>
                            </span>
                       @endif
                    </div>
                    <div class="form-group col-sm-6">
                    <label for="">Date Expiration</label>
                    <input type="date" id="date_expiration" name="date_expiration" class="form-control" value="{{$invoice->date_expiration}}">
                    @if($errors->has('date_expiration'))
                            <span class="text-danger">
                                <strong>{{$errors->first('date_expiration')}}</strong>
                            </span>
                    @endif
                    </div>
                </div>
                

                <div class="row">
                    <div class="form-group col-sm-6">
                    <label for="">State</label>
                    <select  id="state_id" name="state_id" class="select2 form-control" style="width: 100%;">
                        <option value="">Select State</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}" {{$invoice->state_id ? 'selected' : ''}}>{{$state->name2}}</option>
                        @endforeach
                    </select>
                   @if($errors->has('state_id'))
                            <span class="text-danger">
                                <strong>{{$errors->first('state_id')}}</strong>
                            </span>
                   @endif
                    </div>
                    <div class="form-group col-sm-6">
                    <label for="">Rode</label>
                    <input type="text" id="rode" name="rode" class="form-control" placeholder="2000.00" value="{{$invoice->rode}}">
                    @if($errors->has('rode'))
                            <span class="text-danger">
                                <strong>{{$errors->first('rode')}}</strong>
                            </span>
                    @endif
                    </div>
                </div>
                 <div class="col-md-12">
                    <label for="">Archivo Actual</label>
                    <a href="#" onclick="archivo('{{$invoice->file}}')" id="file" >{{$invoice->file}}</a>
                </div>
                <hr>
                
                <div class="col-sm-12">
                  <div class="form-group image">
                    <input  name="file" type="file"  data-preview-file-type="any" class="file"  id="file" value="{{$invoice->file}}">
                  </div>
                  @if($errors->has('file'))
                     <span class="text-danger">
                        <strong>{{$errors->first('file')}}</strong>
                    </span>
                @endif
                  
                </div>

                <center>

                  <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary add">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <a href="{{route('invoice.index')}}" class="btn btn-danger">
                            <span class='glyphicon glyphicon-remove'></span> Regresar
                        </a>
                  </div>
                </center>  
                </form>
              
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->


    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>


</body>
<script type="text/javascript">
            function archivo(archivo){
                window.open(archivo,"width=200","height=100");
             }
</script>
        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')