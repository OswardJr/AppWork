@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

</head>

<body>
    <div class="col-md-12">
        <h2> Edit Task</h2>
        <br />
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Tasks</li>
                </ul>
            </div>
        
            <div class="panel-body">
              <form action="{{route('tasks.update',$task->id)}}" id="form-insert" enctype="multipart/form-data" method="POST"> 
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Company / Client</label>
                            <select id="company_id" name="company_id" class="form-control"  style="width: 100%;">
                                <option value="">Select Company/Client</option>
                                 @foreach($companys as $c)
                                <option value="{{$c->id}}" {{$task->company_id ? 'selected' : ''}}>{{$c->name3}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('company_id'))
                            <span class="text-danger">
                                <strong>{{$errors->first('company_id')}}</strong>
                            </span>
                            @endif
                        </div>
                        @if (auth()->user()->rol === "Admin")                        
                        <div class="form-group col-sm-6">
                            <label>Responsable</label>
                            <select id="user_id" name="user_id" class="form-control"  style="width: 100%;">
                                <option value="">Select Responsable</option>
                                 @foreach($users as $c)
                                <option value="{{$c->id}}" {{$task->user_id ? 'selected' : ''}}>{{$c->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('invoice_number'))
                            <span class="text-danger">
                                <strong>{{$errors->first('invoice_number')}}</strong>
                            </span>
                            @endif
                        </div>
                        @else
                        <div class="form-group col-sm-6">
                            <label>Responsable</label>
                            <select id="user_id" name="user_id" class="form-control"  style="width: 100%;">
                                <option value="{{auth()->user()->id}}">{{auth()->user()->name}}</option>
                            </select>
                            @if($errors->has('invoice_number'))
                            <span class="text-danger">
                                <strong>{{$errors->first('invoice_number')}}</strong>
                            </span>
                            @endif
                        </div>                        
                        @endif                    
                </div>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">Date</label>
                        <input type="date" name="date_task" class="form-control" value="{{$now->format('Y-m-d')}}" value="{{$task->date_task}}">
                       @if($errors->has('date_task'))
                            <span class="text-danger">
                                <strong>{{$errors->first('date_task')}}</strong>
                            </span>
                       @endif
                    </div>
                    <div class="form-group col-sm-6">
                    <label for="">Description</label>
                    <textarea class="form-control" name="description" placeholder="Description" required>{{$task->description}}</textarea>
                    @if($errors->has('description'))
                            <span class="text-danger">
                                <strong>{{$errors->first('description')}}</strong>
                            </span>
                    @endif
                    </div>
                </div>
                

                <div class="row">
                    <div class="form-group col-sm-6">
                    <label for="">State</label>
                    <select  id="state_id" name="state_id" class="select2 form-control" style="width: 100%;">
                        <option value="">Select State</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}" {{$task->state_id ? 'selected' : ''}}>{{$state->name2}}</option>
                        @endforeach
                    </select>
                   @if($errors->has('state_id'))
                            <span class="text-danger">
                                <strong>{{$errors->first('state_id')}}</strong>
                            </span>
                   @endif
                    </div>
                </div>

                <center>
                  <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary add">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <a href="{{route('tasks.index')}}" class="btn btn-danger">
                            <span class='glyphicon glyphicon-remove'></span> Regresar
                        </a>
                  </div>
                </center>  
                </form>
              
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->


    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>


</body>
        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')