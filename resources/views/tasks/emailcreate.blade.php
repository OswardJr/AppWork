@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            </head>

            <body>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <hr>
                            <a class="btn btn-default btn-teal btn-responsive" style="float: right;" href="javascript:history.back(1)" title="Regresar"><i class="fa fa-mail-reply-all fa-lg"></i></a><br>
                    
                              <!-- quick email widget -->
                              <div class="box box-info">
                                <div class="box-header">
                                  <i class="fa fa-envelope"></i>

                                  <h3 class="box-title">Send a Email</h3>
                                  <!-- /. tools -->
                                </div>
                                <div class="box-body">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/emails/') }}">
                                    {{ csrf_field() }}                                    

                                        <div class="form-group{{ $errors->has('ab_name') ? ' has-error' : '' }}">
                                            <label for="ab_name" class="col-md-2 control-label">Name</label>

                                            <div class="col-md-10">
                                                <input id="name" type="text" class="form-control" placeholder="Indique el nombre" name="ab_name" value="{{ old('ab_name') }}" required autofocus>

                                                @if ($errors->has('ab_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ab_name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('ab_email') ? ' has-error' : '' }}">
                                            <label for="ab_email" class="col-md-2 control-label">E-mail</label>

                                            <div class="col-md-10">
                                                <input id="email" type="email" class="form-control" placeholder="Indique el correo electrónico" name="ab_email" value="{{ old('ab_email') }}" required>

                                                @if ($errors->has('ab_email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ab_email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                             

                                        <div class="form-group{{ $errors->has('ab_phone') ? ' has-error' : '' }}">
                                            <label for="ab_phone" class="col-md-2 control-label">Phone</label>

                                            <div class="col-md-10">
                                                <input id="password" type="text" placeholder="584243513322" class="form-control" name="ab_phone" required>

                                                @if ($errors->has('ab_phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ab_phone') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('ab_date') ? ' has-error' : '' }}">
                                            <label for="password-confirm" class="col-md-2 control-label">Date</label>

                                            <div class="col-md-10">
                                                <input id="password-confirm" type="date" placeholder="" value="{{$now->format('Y-m-d')}}" min="{{$now->format('Y-m-d')}}" class="form-control" name="ab_date" required>

                                                @if ($errors->has('ab_date'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ab_date') }}</strong>
                                                </span>
                                                @endif                                                
                                            </div>
                                        </div>
                                    <div>
                                      <textarea class="textarea" placeholder="Message"
                                                style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="ab_message" required></textarea>

                                                @if ($errors->has('ab_message'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('ab_message') }}</strong>
                                                </span>
                                                @endif                                    
                                    </div>
                                <div class="box-footer clearfix">
                                    <button data-toggle="tooltip" title="Guardar" type="submit" class="pull-right btn btn-default" name="agregar">Send <i class="fa fa-arrow-circle-right"></i></button>                                                                </div>                                    
                                  </form>
                                </div>
                              </div>    

                        </div>
                    </div>
                </div>
                <!-- jQuery -->
                <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

            </body>
        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')