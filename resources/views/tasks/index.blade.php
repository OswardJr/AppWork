@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height:1000px">

</head>

<style>
#add_status > .select2-choice{
     background-color:blue;
}   
</style>

<body>
    <div class="col-md-12">
        <h2>Tasks</h2>
        <br />
         @if(session('success'))
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="alert alert-success">{{session('success')}}</div>
        </div>
        @endif
            <a href="{{route('tasks.create')}}" class="add-modal btnn btn btn-default"><i class="fa fa-plus"></i> Add a Task</a>
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Tasks</li>
                </ul>
            </div>
        
            <div class="panel-body">
                    <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th valign="middle">#</th>
                                <th>Company / Client</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>State</th>
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($tasks as $indexKey => $task)
                                <tr class="item{{$task->id}}">
                                    <td class="col1">{{ $indexKey+1 }}</td>
                                    <td>{{$task->name3}}</td>
                                    <td>
                                        {{$task->name}}
                                    </td>
                                    <td>
                                        {{$task->date_task}}
                                    </td>
                                    <td>
                                        {{$task->description}}
                                    </td>
                                    <td>
                                        {{$task->name2}}
                                    </td>                                    
                                    <td>
                                        <button class="show-modal btn btn-primary" data-id="{{$task->id}}" data-company_id="{{$task->name3}}" data-user_id="{{$task->name}}" data-date="{{$task->date_task}}" data-description="{{$task->description}}" data-state_id="{{$task->name2}}">
                                        <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                        <a class="btn btn-warning" href="{{route('tasks.edit',$task->id)}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit</a>

                                        <button class="delete-modal btn btn-danger" data-id="{{$task->id}}" data-company_id="{{$task->name3}}" data-user_id="{{$task->name}}" data-date="{{$task->date_task}}" data-description="{{$task->description}}" data-state_id="{{$task->name2}}">
                                        <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

    <!-- Modal form to add a post -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="form-insert" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="company_id">Company / Client:</label>
                            <div class="col-md-10">
                            <select class="form-control select2" name="company_id" id="company_id" style="width: 100%;" required>
                                <option></option>
                                @foreach($companys as $company)
                                <option style="color: #000;" value="{{$company->id}}">{{$company->name3}}</option>
                                @endforeach
                            </select>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="user_id">Responsable:</label>
                            <div class="col-md-10">
                            <select class="form-control select2" name="user_id" id="user_id" style="width: 100%;" required>
                                <option></option>
                                @foreach($users as $user)
                                <option style="color: #000;" value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="date_task">Date:</label>
                            <div class="col-md-10">
                                <input type="date" class="form-control" name="date_task" id="date_task" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-2" for="description">Description:</label>
                            <div class="col-md-10">
                                <textarea class="form-control" name="description" id="description" placeholder="Description" required></textarea>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="state_id">Status:</label>
                            <div class="col-md-10">
                            <select class="form-control select2" name="state_id" id="state_id" style="width: 100%;" required>
                                <option></option>
                                @foreach($states as $sta)
                                <option style="color: #000;" value="{{$sta->id}}">{{$sta->name2}}</option>
                                @endforeach
                            </select>                                  
                            <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                                                                         
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to show a post -->
    <div id="showModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="company_id">Company / Client:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="show_company_id" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="user_id">Responsable:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="show_user_id" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="date_task">Date:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="show_date_task" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="description">Description:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="show_description" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="state_id">States:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="show_state_id" disabled>
                            </div>
                        </div>                                                
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to edit a form -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_edit" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">Company / Client:</label>
                            <div class="col-md-10">
                            <select class="form-control select2" name="company_id" id="edit_company" style="width: 100%;" required>
                                @foreach($companys as $company)
                                <option style="color: #000;" value="{{$company->id}}">{{$company->name3}}</option>
                                @endforeach
                            </select>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Responsable:</label>
                            <div class="col-md-10">
                            <select class="form-control select2" name="user_id" id="edit_responsable" style="width: 100%;" required>
                                @foreach($users as $user)
                                <option style="color: #000;" value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select> 
                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Date:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="edit_date" placeholder="contact@example.com" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Description:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="edit_description" placeholder="contact@example.com" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">States:</label>
                            <div class="col-md-10">
                            <select class="form-control select2" name="state_id" id="edit_state" style="width: 100%;" required>
                                @foreach($states as $sta)
                                <option style="color: #000;" value="{{$sta->id}}">{{$sta->name2}}</option>
                                @endforeach
                            </select>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                                                                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">¿you want to delete this task?</h3>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="title">Company / Client:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="delete_company" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <script>
        $(window).load(function(){
            $('#mytable').removeAttr('style');
        })
    </script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">

        // Show a post
        $(document).on('click', '.show-modal', function() {
            $('.modal-title').text('Show');
            $('#show_company_id').val($(this).data('company_id'));
            $('#show_user_id').val($(this).data('user_id'));
            $('#show_date_task').val($(this).data('date'));
            $('#show_description').val($(this).data('description'));
            $('#show_state_id').val($(this).data('state_id'));

            $('#showModal').modal('show');
        });
        
        // delete a post
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));
            $('#delete_company').val($(this).data('company_id'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: 'tasks/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    toastr.success('Successfully deleted Task!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data['id']).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>

<!-- <script>
     $(document).ready(function() {

          $("#company_id").select2({
            maximumSelectionLength: 1
          });

          $("#user_id").select2({
            maximumSelectionLength: 1
          });

          $("#state_id").select2({
            maximumSelectionLength: 1
          });

        });
</script> -->
</body>

        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')
