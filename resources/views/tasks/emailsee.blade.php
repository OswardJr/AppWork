@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            </head>

            <body>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">

                            <hr>
                            <a class="btn btn-default btn-teal btn-responsive" style="float: right;" href="javascript:history.back(1)" title="Regresar"><i class="fa fa-mail-reply-all fa-lg"></i></a><br>                 
                            <div class="panel panel-default">
                                <center><div class="panel-heading"><h4>User</h4></div></center>
                                <div class="panel-body">
                    <form method="post" action=" {{ url('/emails/') }}/{{ $mail->id }}">
                        <input name="_method" type="hidden" value="PUT">
                        <input required="true" type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group col-xs-6">
                            <label for="name" class="col-md-4 control-label">Name</label>

                                <input id="name" type="text" class="form-control" placeholder="Indique el nombre" name="name" value="{{ $mail->ab_name }}" required autofocus disabled>
                        </div>

                        <div class="form-group col-xs-6">
                            <label for="email" class="col-md-4 control-label">E-mail</label>

                                <input id="email" type="email" class="form-control" placeholder="Indique el correo electrónico" name="email" value="{{ $mail->ab_email }}" required disabled>

                        </div>  

                        <div class="form-group col-xs-6">
                            <label for="name" class="col-md-4 control-label">Phone</label>

                                <input id="name" type="text" class="form-control" placeholder="Indique el nombre" name="name" value="{{ $mail->ab_phone }}" required autofocus disabled>
                        </div>

                        <div class="form-group col-xs-6">
                            <label for="email" class="col-md-4 control-label">Message</label>

                                <input id="email" type="email" class="form-control" placeholder="Indique el correo electrónico" name="email" value="{{ $mail->ab_message }}" required disabled>

                        </div>                            
          

                            </div> 
                               <center class="col-xs-offset-3 col-xs-6">
                                      <button data-toggle="tooltip" title="Regresar" type="reset" onClick="javascript:history.go(-1);" class="btn btn-refresh margin glyphicon glyphicon-arrow-left"></button>
                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- jQuery -->
                <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

            </body>
        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')