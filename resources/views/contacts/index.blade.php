@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height:1000px">

</head>

<body>
    <div class="col-md-12">
        <h2>Contacts</h2>
        <br />
            <a href="companies" class="btnn btn btn-default"><i class="fa fa-plus"></i> Companies</a>
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Contacts</li>
                </ul>
            </div>
        
            <div class="panel-body">
                    <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th valign="middle">#</th>
                                <th>First Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($contacts as $indexKey => $contact)
                                <tr class="item{{$contact->id}} @if($contact->is_published) warning @endif">
                                    <td class="col1">{{ $indexKey+1 }}</td>
                                    <td>{{$contact->first}}</td>
                                    <td>
                                        {{App\Contact::getExcerpt($contact->phone)}}
                                    </td>
                                    <td>
                                        {{App\Contact::getExcerpt($contact->email)}}
                                    </td>                                    
                                    <td>
                                        <button class="show-modal btn btn-primary" data-id="{{$contact->id}}" data-first="{{$contact->first}}" data-phone="{{$contact->phone}}" data-email="{{$contact->email}}">
                                        <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                        <button class="edit-modal btn btn-warning" data-id="{{$contact->id}}" data-first="{{$contact->first}}" data-phone="{{$contact->phone}}" data-email="{{$contact->email}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        <button class="delete-modal btn btn-danger" data-id="{{$contact->id}}" data-first="{{$contact->first}}" data-phone="{{$contact->phone}}" data-email="{{$contact->email}}">
                                        <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

    <!-- Modal form to add a post -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="form-insert" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">First Name:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="first_add" placeholder="Osward Pacheco" value="{{ old('first') }}" required>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="number" class="form-control" id="phone_add" placeholder="1351923289" value="{{ old('phone') }}" required>
                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Email:</label>
                            <div class="col-md-10">
                                <input type="email" class="form-control" id="email_add" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to show a post -->
    <div id="showModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">First Name:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="first_name" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="phone" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Email:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="email" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal form to edit a form -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_edit" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="first">First Name:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="first_edit" placeholder="Osward Pacheco" value="{{ old('first') }}" required>
                                <p class="errorFirst text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="phone">Phone:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="phone_edit" value="{{ old('phone') }}" required>
                                <p class="errorPhone text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email">Email:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="email_edit" placeholder="contact@example.com" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" value="{{ old('email') }}" required>
                                <p class="errorEmail text-center alert alert-danger hidden"></p>
                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">¿you want to delete this contact?</h3>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="id">ID:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="title">First Name:</label>
                            <div class="col-md-10">
                                <input type="name" class="form-control" id="first_delete" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')

 <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        // add a new post
        $(document).on('click', '.add-modal', function() {
            $('.modal-title').text('Add');
            $('#addModal').modal('show');
        });
        $('.modal-footer').on('click', '.add', function() {
            $.ajax({
                type: 'POST',
                url: 'contacts',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'first': $('#first_add').val(),
                    'phone': $('#phone_add').val(),
                    'email': $('#email_add').val()
                },
                success: function(data) {
                    $('.errorFirst').addClass('hidden');
                    $('.errorPhone').addClass('hidden');
                    $('.errorEmail').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#addModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.first) {
                            $('.errorFirst').removeClass('hidden');
                            $('.errorFirst').text(data.errors.first);
                        }
                        if (data.errors.phone) {
                            $('.errorPhone').removeClass('hidden');
                            $('.errorPhone').text(data.errors.phone);
                        }                        
                        if (data.errors.email) {
                            $('.errorEmail').removeClass('hidden');
                            $('.errorEmail').text(data.errors.email);
                        } 
                    } else {
                        toastr.success('Successfully added Contact!', 'Success Alert', {timeOut: 5000});

                        $('#form-insert')[0].reset();

                        $('#mytable').prepend("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.first + "</td><td>" + data.phone + "</td><td>" + data.email + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-first='" + data.first + "' data-phone='" + data.phone + "' data-email='" + data.email + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-first='" + data.first + "' data-phone='" + data.phone + "' data-email='" + data.email + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-first='" + data.first + "' data-phone='" + data.phone + "' data-email='" + data.email + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                        $('.col1').each(function (index) {
                            $(this).html(index+1);
                        });
                    }
                },
            });
        });

        // Show a post
        $(document).on('click', '.show-modal', function() {
            $('.modal-title').text('Show');
            $('#first_name').val($(this).data('first'));
            $('#phone').val($(this).data('phone'));
            $('#email').val($(this).data('email'));
            $('#showModal').modal('show');
        });


        // Edit a post
        $(document).on('click', '.edit-modal', function() {
            $('.modal-title').text('Edit');
            $('#id_edit').val($(this).data('id'));
            $('#first_edit').val($(this).data('first'));
            $('#phone_edit').val($(this).data('phone'));
            $('#email_edit').val($(this).data('email'));
            id = $('#id_edit').val();
            $('#editModal').modal('show');
        });
        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'PUT',
                url: 'contacts/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#id_edit").val(),
                    'first': $('#first_edit').val(),
                    'phone': $('#phone_edit').val(),
                    'email': $('#email_edit').val()
                },
                success: function(data) {
                    $('.errorFirst').addClass('hidden');
                    $('.errorPhone').addClass('hidden');
                    $('.errorEmail').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#editModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.first) {
                            $('.errorFirst').removeClass('hidden');
                            $('.errorFirst').text(data.errors.first);
                        }
                        if (data.errors.phone) {
                            $('.errorPhone').removeClass('hidden');
                            $('.errorPhone').text(data.errors.phone);
                        }
                        if (data.errors.email) {
                            $('.errorEmail').removeClass('hidden');
                            $('.errorEmail').text(data.errors.email);
                        }                        
                    } else {
                        toastr.success('Successfully updated Contact!', 'Success Alert', {timeOut: 5000});
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.first + "</td><td>" + data.phone + "</td><td>" + data.email + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-first='" + data.first + "' data-phone='" + data.phone + "' data-email='" + data.email + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-first='" + data.first + "' data-phone='" + data.phone + "' data-email='" + data.email + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-first='" + data.first + "' data-phone='" + data.phone + "' data-email='" + data.email + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");

                        $('.col1').each(function (index) {
                            $(this).html(index+1);
                        });
                    }
                }
            });
        });
        
        // delete a post
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));
            $('#first_delete').val($(this).data('first'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: 'contacts/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    toastr.success('Successfully deleted Contact!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data['id']).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>
