@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.css">

<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="height:1000px">

        </head>

        <body>
            <div class="col-md-12">
                <h2>States</h2>
                <br />
                    <a href="#" class="add-modal btnn btn btn-default"><i class="fa fa-plus"></i> Add a State</a>
                <div class="panel panel-primary panel">
                    <div class="panel-heading" style="padding: 20px;">
                        
                            <i class="fa fa-file-text-o"></i> All the current States</li>
                        
                    </div>
                
                    <div class="panel-body">
                            <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                                <thead>
                                    <tr>
                                        <th valign="middle">#</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Actions</th>
                                    </tr>
                                    {{ csrf_field() }}
                                </thead>
                                <tbody>
                                    @foreach($states as $indexKey => $state)
                                        <tr class="item{{$state->id}} @if($state->is_published) warning @endif">
                                            <td class="col1">{{ $indexKey+1 }}</td>
                                            <td>{{$state->name2}}</td>         
                                            <td>{{$state->type}}</td>         
                                            <td>
                                                <button class="show-modal btn btn-primary" data-id="{{$state->id}}" data-name2="{{$state->name2}}" data-type="{{$state->type}}">
                                                <span class="glyphicon glyphicon-eye-open"></span> Show</button>
                                                <button class="edit-modal btn btn-warning" data-id="{{$state->id}}" data-name2="{{$state->name2}}" data-type="{{$state->type}}">
                                                <span class="glyphicon glyphicon-edit"></span> Edit</button>
                                                <button class="delete-modal btn btn-danger" data-id="{{$state->id}}" data-name2="{{$state->name2}}" data-type="{{$state->type}}">
                                                <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div><!-- /.panel-body -->
                </div><!-- /.panel panel-default -->
            </div><!-- /.col-md-8 -->

            <!-- Modal form to add a post -->
            <div id="addModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form" id="form-insert">
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="name2">Name:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="name_add" placeholder="Success" value="{{ old('name2') }}" required>
                                        <p class="errorName text-center alert alert-danger hidden"></p>
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="type">Type:</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="type_add" id="type_add" required>
                                            <option></option>
                                            <option value="Task">Task</option>
                                            <option value="Invoice">Invoice</option>
                                        </select>
                                        <p class="errorType text-center alert alert-danger hidden"></p>
                                    </div>                                    
                                </div>                        
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary add" data-dismiss="modal">
                                    <span id="" class='glyphicon glyphicon-check'></span> Add
                                </button>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    <span class='glyphicon glyphicon-remove'></span> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal form to show a state -->
            <div id="showModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="name2">Name:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" value="{{ old('name2') }}" id="name2" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="type">Type:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" value="{{ old('name') }}" id="type" disabled>
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    <span class='glyphicon glyphicon-remove'></span> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal form to edit a form -->
            <div id="editModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="id">ID:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="id_edit" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="name2">Name:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="name_edit" placeholder="Osward Pacheco" value="{{ old('name2') }}" required>
                                        <p class="errorFirst text-center alert alert-danger hidden"></p>
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="type">Type:</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="type_edit" name="type_edit">
                                            <option>{{$state->states}}</option>
                                            <option value="Task">Task</option>
                                            <option value="Invoice">Invoice</option>
                                        </select>
                                        <p class="errorType text-center alert alert-danger hidden"></p>
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                                    <span class='glyphicon glyphicon-check'></span> Edit
                                </button>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    <span class='glyphicon glyphicon-remove'></span> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal form to delete a form -->
            <div id="deleteModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <h3 class="text-center">¿you want to delete this state?</h3>
                            <br />
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="id">ID:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="id_delete" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="name2">Name:</label>
                                    <div class="col-md-10">
                                        <input type="name" class="form-control" id="name_delete" disabled>
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                    <span id="" class='glyphicon glyphicon-trash'></span> Delete
                                </button>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    <span class='glyphicon glyphicon-remove'></span> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- jQuery -->
            <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

            <script>
                $(window).load(function(){
                    $('#mytable').removeAttr('style');
                })
            </script>

            <!-- AJAX CRUD operations -->
            <script type="text/javascript">
                // add a new contact
                $(document).on('click', '.add-modal', function() {
                    $('.modal-title').text('Add');
                    $('#addModal').modal('show');
                });
                $('.modal-footer').on('click', '.add', function() {
                    $.ajax({
                        type: 'POST',
                        url: 'states',
                        data: {
                            '_token': $('input[name=_token]').val(),
                            'name2': $('#name_add').val(),
                            'type': $('#type_add').val()
                        },
                        success: function(data) {
                            $('.errorName').addClass('hidden');
                            $('.errorType').addClass('hidden');
                            if ((data.errors)) {
                                setTimeout(function () {
                                    $('#addModal').modal('show');
                                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                }, 500);

                                if (data.errors.name) {
                                    $('.errorName').removeClass('hidden');
                                    $('.errorName').text(data.errors.name);
                                }
                                if (data.errors.type) {
                                    $('.errorType').removeClass('hidden');
                                    $('.errorType').text(data.errors.type);
                                }                                                     
                            } else {
                                toastr.success('Successfully added State!', 'Success Alert', {timeOut: 5000});
                                
                                $('#form-insert')[0].reset();                                

                                $('#mytable').prepend("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.name2 + "</td><td>" + data.type + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-name2='" + data.name2 + "' data-type='" + data.type + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-name2='" + data.name2 + "' data-type='" + data.type + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name2='" + data.name2 + "' data-type='" + data.type + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                                $('.col1').each(function (index) {
                                    $(this).html(index+1);
                                });
                            }
                        },
                    });
                });

                // Show a state
                $(document).on('click', '.show-modal', function() {
                    $('.modal-title').text('Show');
                    $('#name2').val($(this).data('name2'));
                    $('#type').val($(this).data('type'));
                    $('#showModal').modal('show');
                });


                // Edit a state
                $(document).on('click', '.edit-modal', function() {
                    $('.modal-title').text('Edit');
                    $('#id_edit').val($(this).data('id'));
                    $('#name_edit').val($(this).data('name2'));
                    $('#type_edit').val($(this).data('type'));
                    id = $('#id_edit').val();
                    $('#editModal').modal('show');
                });
                $('.modal-footer').on('click', '.edit', function() {
                    $.ajax({
                        type: 'PUT',
                        url: 'states/' + id,
                        data: {
                            '_token': $('input[name=_token]').val(),
                            'id': $("#id_edit").val(),
                            'name2': $('#name_edit').val(),
                            'type': $('#type_edit').val()
                        },
                        success: function(data) {
                            $('.errorName').addClass('hidden');
                            $('.errorType').addClass('hidden');

                            if ((data.errors)) {
                                setTimeout(function () {
                                    $('#editModal').modal('show');
                                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                                }, 500);

                                if (data.errors.name2) {
                                    $('.errorName').removeClass('hidden');
                                    $('.errorName').text(data.errors.name2);
                                }                       
                                if (data.errors.type) {
                                    $('.errorType').removeClass('hidden');
                                    $('.errorType').text(data.errors.type);
                                }
                            } else {
                                toastr.success('Successfully updated State!', 'Success Alert', {timeOut: 5000});
                                $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.name2 + "</td><td>" + data.type + "</td><td><button class='show-modal btn btn-primary' data-id='" + data.id + "' data-name2='" + data.name2 + "' data-type='" + data.type + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-warning' data-id='" + data.id + "' data-name2='" + data.name2 + "' data-type='" + data.type + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name2='" + data.name2 + "' data-type='" + data.type + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");

                                $('.col1').each(function (index) {
                                    $(this).html(index+1);
                                });
                            }
                        }
                    });
                });
                
                // delete a state
                $(document).on('click', '.delete-modal', function() {
                    $('.modal-title').text('Delete');
                    $('#id_delete').val($(this).data('id'));
                    $('#name_delete').val($(this).data('name2'));
                    $('#deleteModal').modal('show');
                    id = $('#id_delete').val();
                });
                $('.modal-footer').on('click', '.delete', function() {
                    $.ajax({
                        type: 'DELETE',
                        url: 'states/' + id,
                        data: {
                            '_token': $('input[name=_token]').val(),
                        },
                        success: function(data) {
                            toastr.success('Successfully deleted State!', 'Success Alert', {timeOut: 5000});
                            $('.item' + data['id']).remove();
                            $('.col1').each(function (index) {
                                $(this).html(index+1);
                            });
                        }
                    });
                });
            </script>

        <script>
            $(document).ready(function() {
                $('#mytable').dataTable();
            } );    
        </script>  
        </body>

        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')