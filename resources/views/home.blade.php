@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
    	<div class="content-wrapper">
    		<section class="content-header">
		      <h1>
		        Dashboard
		        <small>Control panel</small>
		      </h1>
		      <ol class="breadcrumb">
		        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		        <li class="active">Dashboard</li>
		      </ol>
		    </section>
				<div class="row">
		    	    @if(Session::get('message'))
					    <div class="col-md-8 col-md-offset-2">
					      <div class="alert alert-success alert-dismissable">
					        <button type="button" class="close" style="color: black" data-dismiss="alert" aria-hidden="true">&times;</button>
					        <center><strong><h3> {{ Session::get('message') }}</h3></strong></center>
					      </div>
					    </div>
					    @endif			
				</div>		    

		    <!-- Main content -->
		    <section class="content">
		      <!-- Small boxes (Stat box) -->
		      <div class="row">
		        <div class="col-lg-3 col-xs-6">
		          <!-- small box -->
		          <div class="small-box bg-aqua">
		            <div class="inner">
		              <h3><?php echo $one; ?></h3>

		              <p>Number of Companies</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-bag"></i>
		            </div>
		            <a href="{{route('companies.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		          </div>
		        </div>
		        <!-- ./col -->
		        <div class="col-lg-3 col-xs-6">
		          <!-- small box -->
		          <div class="small-box bg-green">
		            <div class="inner">
		              <h3><?php echo $two; ?><sup style="font-size: 20px"></sup></h3>

		              <p>Number of Clients</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
		            <a href="{{route('companies.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		          </div>
		        </div>
		        <!-- ./col -->
		        <div class="col-lg-3 col-xs-6">
		          <!-- small box -->
		          <div class="small-box bg-yellow">
		            <div class="inner">
		              <h3><?php echo $three; ?></h3>

		              <p>Number of Tasks</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-person-add"></i>
		            </div>
		            <a href="{{route('tasks.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		          </div>
		        </div>
		        <!-- ./col -->
		        <div class="col-lg-3 col-xs-6">
		          <!-- small box -->
		          <div class="small-box bg-red">
		            <div class="inner">
		              <h3><?php echo $fourth; ?></h3>

		              <p>Number of Emails</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-pie-graph"></i>
		            </div>
		            <a href="{{route('emails.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		          </div>
		        </div>
		        <!-- ./col -->
		      </div>
		      <!-- /.row -->
		      <!-- Main row -->
		      <div class="row">
		        <!-- Left col -->

		        <!-- /.Left col -->
		        <!-- right col (We are only adding the ID to make the widgets sortable)-->
		        <!-- right col -->
		      </div>
		      <!-- /.row (main row) -->
    </section>

	    </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')