@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.css">

    <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>    

    <header class="main-header">
        @include('layouts.header')
    </header>

    @include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    </head>

    <body>
      <section class="content">
        <div class="col-md-12">
            <h2>Users</h2>
            <br />
            <a href="users/create" class="add-modal btnn btn btn-default"><i class="fa fa-plus"></i> Add a User</a>
            <div class="panel panel-primary panel">
                <div class="panel-heading">
                    <ul>
                        <li><i class="fa fa-file-text-o"></i> All the current Users</li>
                    </ul>
                </div>
                
                <div class="panel-body">
                    <table class="table table-responsive table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th valign="middle">#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th colspan="3">Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @foreach($usuarios as $indexKey => $usu)
                            <tr class="item{{$usu->id}}">
                                <td class="col1">{{ $indexKey+1 }}</td>
                                <td>{{$usu->name}}</td>
                                <td>
                                    {{$usu->email}}
                                </td>                                                                    
                                <td>
                                  <a href="{{ route('users.show', $usu->id) }}" class="btn btn-primary" data-toggle="tooltip" title="Show"><i class="fa fa-eye"></i>

                                  </a>
                              </td>  
                              
                              <td>
                                  <a href="{{ route('users.edit', $usu->id) }}" class="btn btn-warning" data-toggle="tooltip" title="Update"><i class="fa fa-pencil"></i>
                                  </a>
                              </td>  

                              <td>
                                  <form action="{{ route('users.destroy', $usu->id) }}" method="post">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input required="true" type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" onclick="if(!confirm('¿Desea eliminar al analista?'))event.preventDefault();" class="btn btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></button>

                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->
</section><!-- /.content -->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('#mytable').dataTable();
    } );    
</script> 

</body>
</div>  
<!-- /.content-wrapper -->

@include('layouts.footer')