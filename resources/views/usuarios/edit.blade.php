@include('layouts.head')

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

<header class="main-header">
@include('layouts.header')
</header>

@include('layouts.menu')

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            </head>

            <body>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <hr>
                            <a class="btn btn-default btn-teal btn-responsive" style="float: right;" href="javascript:history.back(1)" title="Regresar"><i class="fa fa-mail-reply-all fa-lg"></i></a><br>            
                            <div class="panel panel-default">
                                <center><div class="panel-heading"><h4>Edit User</h4></div></center>                                
                                <div class="panel-body">
                                    <form method="post" action=" {{ url('/users/') }}/{{ $usuarios->id }}" class="form-horizontal" role="form">
                                     <input name="_method" type="hidden" value="PUT">
                                     <input required="true" type="hidden" name="_token" value="{{ csrf_token() }}">

                                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Name</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" placeholder="Indique el nombre" name="name" value="{{ old('name') }}{{ $usuarios->name }}" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-mail</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" placeholder="Indique el correo electrónico" name="email" value="{{ old('email') }}{{ $usuarios->email }}" required>

                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Password</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" placeholder="Indique el correo electrónico" name="password" value="{{ old('password') }}{{ $usuarios->password }}" required readonly>

                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>                                    
                                   

                                    <div class="col-md-10 col-md-offset-2">
                                          <h3>Permits</h3>
                                          <p>Section the functions to which the user will have access</p>            
                                          <table class="table">
                                            <thead>
                                              <tr>
                                                <th>Module / Permission</th>
                                                <th>Habilitar</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($modules as $mod)
                                              <tr>
                                                <td>{{ ($mod->nombre) }}</td>
                                                <td>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" 
                                                               value="{{ ($mod->id) }}" 
                                                               name="modules[]"
                                                                @if (in_array($mod->id, $checked ))
                                                                    {{ 'checked' }}
                                                                @endif  
                                                        ></label>
                                                </div>
                                                </td>
                                              </tr>
                                            @endforeach
                                            </tbody>
                                          </table>
                                        </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <center>                                      
                                                <button data-toggle="tooltip" title="Guardar" type="submit" class="btn btn-guardar margin glyphicon glyphicon-floppy-disk" name="agregar"></button>
                                            </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <!-- jQuery -->
                <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

            </body>
        </div>  
    <!-- /.content-wrapper -->

@include('layouts.footer')