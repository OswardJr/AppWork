<?php

namespace App;
use App\User;
use App\Company;
use App\State;


use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	protected $fillable=[
		'company_id','user_id','date_task','description','state_id',
	];	
    public function company(){
    	return $this->belongsTo(Company::class);
    }
    public function state(){
    	return $this->belongsTo(State::class);
    }
    public function user(){
    	return $this->belongsTo(User::class);
    }    
}
