<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer;
use App\Task;
use App\Email;
use DateTime;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{

    public function index()
    {
        if (auth()->user()->rol === "Admin") {
            $mail = DB::table('emails')->paginate(8);               
        }

        if (auth()->user()->rol === "Seller"){
            $este = auth()->user()->id;
    
            $mail = DB::select('
            SELECT
            i.*,
            b.*
            FROM
                emails AS i
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            WHERE
                i.user_id = "'.$este.'"    
            ');
        }
        
        return view('tasks.emailindex', compact('mail'));
    }

    private $path = 'tasks';

    public function create()
    {
        // $tasks = DB::select('
        // SELECT
        // i.company_id,i.date_task,
        // a.name,a.email
        // FROM
        // tasks AS i
        // INNER JOIN companies AS a
        // ON
        // i.company_id = a.id
        // WHERE
        // i.date_task = "2018/05/15"
        // ');                

        // foreach ($tasks as $tas) {
        //     # code...
        // }

        // $mytime = Carbon\Carbon::now();

        // if ($tas->date_task = $mytime) {       
        
        //     $mail = new PHPMailer\PHPMailer();
        //     $mail->IsSMTP(); 
        //     $mail->SMTPAuth = true;
        //     $mail->SMTPSecure = 'tls';
        //     $mail->Host = 'email-smtp.eu-west-1.amazonaws.com';
        //     $mail->Port = 587;
        //     $mail->IsHTML(true);
        //     $mail->Username = "AKIAJHX53OMEYW2ZGLLQ";
        //     $mail->Password = "AgaDszOHbvVabhdEGmA91kljAeS0ni3qfSZ3TNmL5CQ1";
        //     $mail->setFrom('opacheco@efiempresa.com', 'Efiempresa,S.A');
        //     $mail->Subject = "Test Subject";
        //     $mail->Body = "YEAAAAHHHH";
        //     $mail->AddAddress($tas->email, "Hola");
            
        //     $exito = $mail->Send();      
            
        //     $intentos=1;  
        //         while ((!$exito) && ($intentos < 5)) { 
        //         sleep(5); 
        //             //echo $mail->ErrorInfo; 
        //             $exito = $mail->Send(); 
        //             $intentos=$intentos+1;     
        //         } 
        //            if(!$exito) 
        //         { 
        //            echo "Problemas enviando correo electrónico"; 
        //            echo "<br>".$mail->ErrorInfo;     
        //         }else{ 
        //             echo "<br>Mensaje enviado correctamente<br>"; 
            
        //             $r = Array($tas->email);

        //         } 

        //     }else{
        //         $mail = new PHPMailer\PHPMailer();
        //         $mail->IsSMTP(); 
        //         $mail->SMTPAuth = true; // authentication enabled
        //         $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for aws
        //         $mail->Host = 'email-smtp.eu-west-1.amazonaws.com';
        //         $mail->Port = 587; // or 587
        //         $mail->IsHTML(true);
        //         $mail->Username = "AKIAJHX53OMEYW2ZGLLQ";
        //         $mail->Password = "AgaDszOHbvVabhdEGmA91kljAeS0ni3qfSZ3TNmL5CQ1";
        //         $mail->setFrom('opacheco@efiempresa.com', 'Efiempresa,S.A');
        //         $mail->Subject = "Test Subject";
        //         $mail->Body = "NOOOOOO";
        //         $mail->AddAddress("ojpr15@gmail.com", "Hola");
        //         if ($mail->Send()) {
        //             return 'Email Sended Successfully';
        //         } else {
        //             return 'Failed to Send Email';
        //         }
        //     }

        $now=Carbon::now();

        return view('tasks.emailcreate', compact('now'));        
    }

    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'ab_name' => 'required',
            'ab_email' => 'required',
            'ab_phone' => 'required',
            'ab_date' => 'required',
            'ab_message' => 'required'
        ])->validate();

        $mails = new Email();
        $mails->ab_name = $request->ab_name;
        $mails->ab_email = $request->ab_email;
        $mails->ab_phone = $request->ab_phone;
        $mails->ab_date = $request->ab_date;
        $mails->ab_message = $request->ab_message;
        $iduser = Auth::user()->id;
        $mails->user_id = $iduser;        
        $mails->save();     

        $now=Carbon::now()->format('Y-m-d');
        $fecha = $request->ab_date;

        if ($now == $fecha) {
            $mail = new PHPMailer\PHPMailer();
            $mail->IsSMTP(); 
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'email-smtp.eu-west-1.amazonaws.com';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->Username = "AKIAJHX53OMEYW2ZGLLQ";
            $mail->Password = "AgaDszOHbvVabhdEGmA91kljAeS0ni3qfSZ3TNmL5CQ1";
            $mail->setFrom('opacheco@efiempresa.com', 'AppWork Cartera');
            $mail->Subject = "Probando Factura Email";
            $mail->Body = ($request->ab_message);
            $mail->AddAddress($request->ab_email, $request->ab_name);
            
            $exito = $mail->Send();      
            
            $intentos=1;  
                while ((!$exito) && ($intentos < 5)) { 
                sleep(5); 
                    //echo $mail->ErrorInfo; 
                    $exito = $mail->Send(); 
                    $intentos=$intentos+1;     
                } 
                   if(!$exito) 
                { 
                    echo '<script type="text/javascript">alert("Error");</script>'; 
                   echo "<br>".$mail->ErrorInfo;     
                }else{ 
                    echo '<script type="text/javascript">alert("Success");</script>'; 
            
                    $r = Array($request->ab_email);

                } 
            }else{
                echo "nada";
            }           

        $mail = DB::table('emails')->paginate(5); 
        
        $now=Carbon::now();

        return view('tasks.emailcreate', compact('mail', 'now'));
    }

    public function show($id)
    {
        $mail = Email::findOrFail($id);

        return view($this->path . '.emailsee', compact('mail'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $mail = Email::findOrFail($id);
        $mail->delete();

        $mail = DB::table('emails')->orderBy('id', 'desc')->paginate(15);

        return view('tasks.emailindex', ['mail' => $mail]);
    }
}
