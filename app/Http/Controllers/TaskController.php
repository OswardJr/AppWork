<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Auth;
use Carbon\Carbon;
use App\Company;
use App\User;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }    

    public function index()
    {
        if (auth()->user()->rol === "Admin") {
            $tasks = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            ');
        }

        if (auth()->user()->rol === "Seller"){
            $este = auth()->user()->id;

            $tasks = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            WHERE
                i.user_id = "'.$este.'"    
            ');            
        }  

        // $user = json_decode($response->getBody()->getContents())[0];

        $companys= Company::orderBy('id',"desc")->get();        
        $users= DB::table('users')->orderBy('id',"desc")->get();        
        $states = DB::table('states')->where('type', 'Task')->get();               

        $permiso = new PermisosController;
        $permisos = $permiso->permisos(5);

        if ($permisos) {
            return view('tasks.index', compact('tasks','companys','users','states'));
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }  
    }

    public function email()
    {
        return view('tasks.email');
    }    

    public function create()
    {
        if (auth()->user()->rol === "Admin") {
            $tasks = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            ');
        }

        if (auth()->user()->rol === "Seller"){
            $este = auth()->user()->id;

            $tasks = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            WHERE
                i.user_id = "'.$este.'"    
            ');            
        }  

        // $user = json_decode($response->getBody()->getContents())[0];
        $now=Carbon::now();
        $companys= Company::orderBy('id',"desc")->get();        
        $users= DB::table('users')->orderBy('id',"desc")->get();        
        $states = DB::table('states')->where('type', 'Task')->get();

        return view('tasks.create',compact('companys','now','users','states'));
    }

    public function store(Request $request)
    {
        $tasks=task::create($request->all());
        $tasks->save();

        return redirect()->route('tasks.index')->with('success','Task saved successfully');
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);

        return view('tasks.show', ['task' => $task]);
    }

    public function edit($id)
    {
        if (auth()->user()->rol === "Admin") {
            $tasks = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            ');
        }

        if (auth()->user()->rol === "Seller"){
            $este = auth()->user()->id;

            $tasks = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            WHERE
                i.user_id = "'.$este.'"    
            ');            
        }  

        // $user = json_decode($response->getBody()->getContents())[0];
        $now=Carbon::now();
        $task=Task::findOrFail($id);
        $companys= Company::orderBy('id',"desc")->get();        
        $users= DB::table('users')->orderBy('id',"desc")->get();        
        $states = DB::table('states')->where('type', 'Task')->get();

        return view('tasks.edit',compact('tasks','now','companys','states','task','users'));
    }

    public function update(Request $request, $id)
    {
        $task=Task::findOrFail($id);
        $task->update($request->all());
        $task->save();

        return redirect()->route('tasks.index')->with('success','Task successfully edited');
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return response()->json($task);
    }
}
