<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PermisosController;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    private $path = 'usuarios';

    public function index() {
        $usuarios = DB::table('users')->orderBy('id', 'desc')->paginate(15);

        $permiso = new PermisosController;
        $permisos = $permiso->permisos(6);

        if ($permisos) {
            return view('usuarios.index', ['usuarios' => $usuarios]);
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }
    }

    public function create() {
        $modules = DB::select('select * from modules');

        $permiso = new PermisosController;
        $permisos = $permiso->permisos(6);
        if ($permisos) {
            return view('usuarios.create', ['modules' => $modules]);
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }
    }
    public function seguridad() {
        $usuarios = User::findOrFail(Auth::user()->id);

        return view('usuarios.seguridad', ['usuarios' => $usuarios]);
    }

    public function cambio_clave(Request $request) {

        $user = User::findOrFail(Auth::user()->id);

        if (Hash::check(Input::get('password'), $user->password)) {
            return redirect('/seguridad')->with('vieja', '¡Contraseña incorrecta!');
        }

        Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ])->validate();

        $usuarios = User::findOrFail(Auth::user()->id);
        $usuarios->password = Hash::make(Input::get('password'));
        $usuarios->save();

        $permiso = new PermisosController;

        $permiso->bitacora('Cambió su clave');

        return redirect('/seguridad')->with('message', '¡Contraseña guardada exitosamente!');
    }

    public function store(Request $request) {

        Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ])->validate();

        $usuarios = new User();
        $usuarios->name = $request->name;
        $usuarios->email = $request->email;
        $usuarios->password = Hash::make(Input::get('password'));
        $usuarios->rol = 'Seller';        
        $usuarios->save();

        $InsertId = $usuarios->id;

        $input = $request->all();

        $modules = $input['modules'];

        foreach ($modules as $mod) {
            DB::insert('insert into mod_usu (id_usu, id_mod) values (?, ?)', [$InsertId, $mod]);
        }

        $usuarios = DB::table('users')->orderBy('id', 'desc')->paginate(15);

        return view('usuarios.index', ['usuarios' => $usuarios]);
    }

    public function edit($id) {
        //  $userid = Auth::user()->id;

        $modules = DB::select('SELECT * from modules ');

        $checkeados = DB::select('SELECT modules.id as id FROM `mod_usu` INNER JOIN modules, users where modules.id = mod_usu.id_mod and users.id = mod_usu.id_usu and id_usu = ?', [$id]);

        $checked = array();

        foreach ($checkeados as $key) {
            array_push($checked, $key->id);
        }

        $usuarios = User::findOrFail($id);

        $permiso = new PermisosController;
        $permisos = $permiso->permisos(6);
        
        if ($permisos) {
            return view('usuarios.edit', compact('usuarios', 'modules'), ['checked' => $checked]);
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }
    }

    public function show($id) {
        $modules = DB::select('SELECT * from modules ');

        $checkeados = DB::select('SELECT modules.id as id FROM `mod_usu` INNER JOIN modules, users where modules.id = mod_usu.id_mod and users.id = mod_usu.id_usu and id_usu = ?', [$id]);

        $checked = array();

        foreach ($checkeados as $key) {
            array_push($checked, $key->id);
        }

        $usuarios = User::findOrFail($id);
        return view($this->path . '.see', compact('usuarios', 'modules'), ['checked' => $checked]);
    }

    public function update(Request $request, $id) {
        Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ])->validate();

        $usuarios = User::findOrFail($id);
        $usuarios->name = $request->name;
        $usuarios->email = $request->email;
        $usuarios->password = $request->password;
        $usuarios->rol = 'Seller';                
        $usuarios->save();

        $input = $request->all();

        $modules = $input['modules'];

        DB::delete('delete from mod_usu where id_usu = ?', [$id]);

        foreach ($modules as $mod) {
            DB::insert('insert into mod_usu (id_usu, id_mod) values (?, ?)', [$id, $mod]);
        }
        
        $usuarios = DB::table('users')->orderBy('id', 'desc')->paginate(15);

        return view('usuarios.index', ['usuarios' => $usuarios]);

    }

    public function destroy($id) {
        $usuarios = User::findOrFail($id);
        $usuarios->delete();

        $usuarios = DB::table('users')->orderBy('id', 'desc')->paginate(15);

        return view('usuarios.index', ['usuarios' => $usuarios]);        
    }
}
