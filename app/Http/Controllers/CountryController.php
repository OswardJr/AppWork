<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Response;
use App\Country;
use View;

class CountryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
        
    protected $rules =
    [
        'Codigo' => 'required',
        'Pais' => 'required'
    ];

    public function index()
    {
        $countries = Country::orderBy('id', 'asc')->get();

        return view('countries.index', ['countries' => $countries]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $countries = new Country();
            $countries->Codigo = $request->Codigo;
            $countries->Pais = $request->Pais;
            $countries->save();
            return response()->json($countries);
        }
    }

    public function show($id)
    {
        $countries = Country::findOrFail($id);

        return view('countries.show', ['countries' => $countries]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $countries = Country::findOrFail($id);
            $countries->Codigo = $request->Codigo;
            $countries->Pais = $request->Pais;
            $countries->save();
            return response()->json($countries);
        }
    }

    public function destroy($id)
    {
        $countries = Country::findOrFail($id);
        $countries->delete();

        return response()->json($countries);
    }    
}
