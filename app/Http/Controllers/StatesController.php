<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Response;
use App\State;
use View;

class StatesController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
        
    protected $rules =
    [
        'name2' => 'required',
        'type' => 'required'    
    ];

    public function index()
    {
        $states = State::orderBy('id', 'desc')->get();

        $permiso = new PermisosController;
        $permisos = $permiso->permisos(1);

        if ($permisos) {
            return view('states.index', ['states' => $states]);
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }        
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $state = new State();
            $state->name2 = $request->name2;
            $state->type = $request->type;
            $state->save();
            return response()->json($state);
        }
    }

    public function show($id)
    {
        $state = State::findOrFail($id);

        return view('state.show', ['state' => $state]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $state = State::findOrFail($id);
            $state->name2 = $request->name2;
            $state->type = $request->type;            
            $state->save();
            return response()->json($state);
        }
    }

    public function destroy($id)
    {
        $state = State::findOrFail($id);
        $state->delete();

        return response()->json($state);
    }
}
