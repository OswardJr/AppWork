<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use Response;
use App\Contact;
use View;
use Auth;
use App\User;

class ContactsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
        
    protected $rules =
    [
        'first' => 'required',
        'phone' => 'required',
        'email' => 'required|email',
    ];

    public function index()
    {
        $contacts = Contact::orderBy('id', 'desc')->get();

        return view('contacts.index', ['contacts' => $contacts]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
       
            $contact = new Contact();
            $contact->first = $request->first;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->save();

            $contact->companies()->sync($request->get('company_id'));
            return redirect()->route('companies.index')->with('success','Contact guardado con exito');
        
    }

    public function show($id)
    {
        $contact = Contact::findOrFail($id);

        return view('contact.show', ['contact' => $contact]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $contact = Contact::findOrFail($id);
            $contact->first = $request->first;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->save();
            return response()->json($contact);
        }
    }

    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return response()->json($contact);
    }
}
