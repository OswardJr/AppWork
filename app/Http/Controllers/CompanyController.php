<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class CompanyController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    protected $rules =
    [
        'identification' => 'required',
        'name3' => 'required',
        'country' => 'required',
        'address' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'url' => 'required',
        'type' => 'required'
    ];


    private $path = 'company';

    public function index()
    {
        $countries= DB::table('countries')->orderBy('Pais',"asc")->get();
        $contacts= DB::table('contacts')->orderBy('id',"desc")->get();

        if (auth()->user()->rol === "Admin") {
            $companies = Company::orderBy('id','DESC')->get();
        }

        if (auth()->user()->rol === "Seller"){
            $id_user=auth()->user()->id;

            $companies = Company::orderBy('id','DESC')->whereHas('users', function($query) use($id_user){
                $query->where('user_id',$id_user);
            })->get();            
        }
        
        $permiso = new PermisosController;
        $permisos = $permiso->permisos(3);

        if ($permisos) {
            return view('companies.index', compact('companies','contacts','countries'));
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }  
    }

    public function create()
    {   

    }

    public function store(Request $request)
    { 
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {        
        $company=new Company();
        $company->id;
        $company->identification = $request->identification;
        $company->name3 = $request->name3;
        $company->country = $request->country;
        $company->address = $request->address;
        $company->phone = $request->phone;
        $company->email = $request->email;
        $company->url = $request->url;
        $company->type=$request->type;
        $company->save();
        $company->id;
        //id user loggin
        $iduser = Auth::user()->id;
        $company->users()->sync($iduser);

        return response()->json($company);

        }
    }

    public function show($id)
    {
        $id_user=auth()->user()->id;

        if (auth()->user()->rol === "Admin") {
            $companies = Company::findOrFail($id);
            $companies = Company::orderBy('id','DESC')->where('id', $id)->get();
        }

        if (auth()->user()->rol === "Seller"){
            $id_user=auth()->user()->id;

            $companies = Company::orderBy('id','DESC')->whereHas('users', function($query) use($id_user){
                $query->where('user_id',$id_user);
            })->where('id', $id)->get();            
        }

        return view('companies.create',compact('companies'));
    }

    public function edit($id)
    {

        $counts = findOrFail($id);
            $counts = DB::select('
                SELECT i.contact_id, contacts.first
                FROM global as i
                inner JOIN companies as q
                ON i.company_id = q.id
                INNER JOIN contacts
                ON i.contact_id = contacts.id
                WHERE company_id');
    }

    public function update(Request $request, $id)
    {       
        $company = Company::findOrFail($id);
        $company->identification = $request->identification;
        $company->name3 = $request->name3;
        $company->country = $request->country;
        $company->address = $request->address;
        $company->phone = $request->phone;
        $company->email = $request->email;
        $company->url = $request->url;
        $company->type=$request->type;
        $company->save();     

        $iduser = Auth::user()->id;
        $company->users()->sync($iduser);

        return response()->json($company);
    }

    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        $company->delete();

        return response()->json($company);
    }
}


