<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use Auth;
use App\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;

class ReportController extends Controller
{
    public function index()
    {
        $reports= DB::table('companies')->where('type', 'Provider')->orderBy('id',"desc")->get();        

        return view('reports.companies', compact('reports'));
    }

    public function create()
    {
        $reports2= DB::table('companies')->where('type', 'Client')->orderBy('id',"desc")->get();        

        return view('reports.clients', compact('reports2'));
    }

    public function uno(Request $request) {

        $reports= DB::table('companies')->where('type', 'Provider')->orderBy('id',"desc")->get();        

        view()->share('reports', $reports);

        if ($request->has('download')) {
            $pdf = PDF::loadView('reports/pdf1');
            return $pdf->stream('All-Companies.pdf');
        }
    }

    public function dos(Request $request) {

        $reports2= DB::table('companies')->where('type', 'Client')->orderBy('id',"desc")->get();        

        view()->share('reports2', $reports2);

        if ($request->has('download')) {
            $pdf = PDF::loadView('reports/pdf2');
            return $pdf->stream('All-Clients.pdf');
        }
    }
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
