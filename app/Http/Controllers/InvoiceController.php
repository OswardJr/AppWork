<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\User;
use App\Company;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Http\Requests\InvoiceStoreRequest;
use App\Http\Requests\InvoiceUpdateRequest;


class InvoiceController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function index()
    {

        $invoices = DB::select('
        SELECT
        i.*,
        a.name2,
        c.name3
        FROM
            invoices AS i
        INNER JOIN states AS a
        ON
            i.state_id = a.id
        INNER JOIN companies AS c
        ON
            i.company_id = c.id
            '); 

        $now=Carbon::now();
        $companies = Company::orderBy('id','ASC')->get();
        $states = DB::table('states')->where('type', 'Invoice')->get();

        $permiso = new PermisosController;
        $permisos = $permiso->permisos(4);

        if ($permisos) {
            return view('invoices.index', compact('companies','now','invoices','states'));
        } else {
            return redirect('/home')->with('message', '¡Acceso no permitido, contacte al administrador!');
        }         
    }

    public function create(){
        $now=Carbon::now();
        $companies = Company::orderBy('id','ASC')->get();
        $states = DB::table('states')->where('type', 'Invoice')->get();
        return view('invoices.create',compact('companies','now','states'));
    }

    public function store(InvoiceStoreRequest $request)
    {
        $invoice=Invoice::create($request->all());
        
        if($request->file('file')){
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = public_path() .'/img/';    
            $img='img/'.$name;
            $file->move($path,$name);            
            $invoice->fill(['file'=> asset($img)])->save();
        }
       

        return redirect()->route('invoice.index')->with('success','Invoice guardado con exito');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $invoice=Invoice::findOrFail($id);
        $now=Carbon::now();
        $companies = Company::orderBy('id','ASC')->get();
        $states = DB::table('states')->where('type', 'Invoice')->get();
        return view('invoices.edit',compact('invoice','now','companies','states'));
    }

    public function update(InvoiceUpdateRequest $request, $id)
    {

        $invoice=Invoice::findOrFail($id);
        $invoice->update($request->all());
        
         if($request->file('file')){
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = public_path() .'/img/';    
            $img='img/'.$name;
            $file->move($path,$name);            
            $invoice->fill(['file'=> asset($img)])->save();
        }

        return redirect()->route('invoice.index')->with('success','Invoice editado con exito');
    }

    public function destroy($id)
    {
     return view('invoices.create');
    }

    public function invoiceBorrar($id){
        Invoice::findOrFail($id)->delete();
        return redirect()->route('invoice.index')->with('success','Invoice eliminada con exito');
    }
}
