<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Support\Facades\DB;

class PermisosController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }
    public function permisos($modulo) {
        $id = Auth::user()->id;
        $permiso = DB::table('mod_usu')
            ->select(DB::raw('count(mod_usu.id) as numero'))
            ->join('users', 'mod_usu.id_usu', '=', 'users.id')
            ->where('id_usu', '=', $id)
            ->where('id_mod', '=', $modulo)
            ->first();
        if ($permiso->numero >= 1) {
            return true;
        } elseif ($permiso->numero < 1) {
            return false;
        }
    }

}
