<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Company;
use Auth;
use App\User;
use App\Task;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        if (auth()->user()->rol === "Admin") {
            $one= DB::table('companies')->where('type', 'Provider')->count();        

            $two= DB::table('companies')->where('type', 'Client')->count();        

            $three= DB::table('tasks')->count();        

            $fourth= DB::table('emails')->count();         
        }

        if (auth()->user()->rol === "Seller"){

            $id_user=auth()->user()->id;

            //count 1
            $one = Company::orderBy('id','DESC')->whereHas('users', function($query) use($id_user){
                $query->where('user_id',$id_user);
            })->where('type', 'Provider')->count();


            //count 2
            $two = Company::orderBy('id','DESC')->whereHas('users', function($query) use($id_user){
                $query->where('user_id',$id_user);
            })->where('type', 'Client')->count();                        
       

            //count 3
            $este = auth()->user()->id;

            $array = DB::select('
            SELECT
            i.*,
            a.name2,
            b.name,
            c.name3
            FROM
                tasks AS i
            INNER JOIN states AS a
            ON
                i.state_id = a.id
            INNER JOIN users AS b
            ON
                i.user_id = b.id
            INNER JOIN companies AS c
            ON
                i.company_id = c.id
            WHERE
                i.user_id = "'.$este.'"    
            ');

            $three = count($array);


            //count 4
            $aste = auth()->user()->id;

            $array2= DB::select('
            SELECT
            a.*,
            b.id
            FROM
                emails AS a
            INNER JOIN users as b
            ON
                a.user_id = b.id
            WHERE
                a.user_id = "'.$aste.'"                     
            ');

            $fourth = count($array2); 
        }       

        return view('home', compact('one', 'two', 'three', 'fourth'));
    }    
}
