<?php

namespace App;
use App\Company;
use App\State;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

	protected $fillable=[
		'company_id','state_id','invoice_number','date_issue','date_expiration','rode','file',
	];
    public function company(){
    	return $this->belongsTo(Company::class);
    }

    public function state(){
    	return $this->belongsTo(State::class);
    }
}
