<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\User;

class Company extends Model
{
    protected $table = 'companies';	
    protected $fillable=[
    	'identification','name','country','address','phone','email','url','type',
    ];

    public function contacts(){
    	return $this->belongsToMany(Contact::class,'global');
    }

   public function users(){
   	 	return $this->belongsToMany(User::class);
   }
}
