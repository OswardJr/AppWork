<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('contact_id')->unsigned();

            //Relations
            $table->foreign('company_id')->references('id')->on('companies')
            ->onDelete('cascade')
            ->onCascade('cascade');
              $table->foreign('contact_id')->references('id')->on('contacts')
            ->onDelete('cascade')
            ->onCascade('cascade');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global');
    }
}
