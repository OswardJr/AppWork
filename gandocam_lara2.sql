-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 01, 2018 at 05:24 AM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gandocam_lara2`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `identification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Client','Provider') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `identification`, `name3`, `country`, `address`, `phone`, `email`, `url`, `type`, `created_at`, `updated_at`) VALUES
(1, '534545', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'www.oswardme.com', 'Client', '2018-05-03 21:58:15', '2018-05-03 21:58:15'),
(2, '54545', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'www.google.com', 'Client', '2018-05-03 22:01:23', '2018-05-03 22:01:23'),
(3, '34567965432', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'jacobo.ramirez@gmail.com', 'hdjfyf', 'Client', '2018-05-03 22:05:01', '2018-05-03 22:05:01'),
(4, '4454545', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'jperez@efiempresa.com', 'frrgrg', 'Client', '2018-05-03 22:08:29', '2018-05-03 22:08:29'),
(6, '5678765', 'hola', 'Afghanistan', 'jkdjdjk', '3434343', 'leonciorequena1995@gmail.com', 'dfdfdf', 'Client', '2018-05-03 23:11:24', '2018-05-03 23:11:24'),
(7, '43434', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'jacobo.ramirez@gmail.com', 'frf', 'Client', '2018-05-03 23:12:43', '2018-05-03 23:12:43'),
(9, '64758475', 'jrhcocoh', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Provider', '2018-05-04 01:03:16', '2018-05-04 01:03:16'),
(10, '64758475', 'ttttttt', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Provider', '2018-05-04 01:04:24', '2018-05-04 01:04:24'),
(11, '64758475', 'FVVTVVV', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Provider', '2018-05-04 01:06:21', '2018-05-04 01:06:21'),
(37, '64758475', 'ghgujb', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Provider', '2018-05-04 08:21:57', '2018-05-04 08:21:57'),
(36, '87657689', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'hvjkbkkjb', 'Client', '2018-05-04 08:14:51', '2018-05-04 08:14:51'),
(14, '64758475', 'uuuuuuuuuu', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Provider', '2018-05-04 01:25:29', '2018-05-04 01:25:29'),
(26, 'aaaaaaa', 'yeli', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Client', '2018-05-04 05:48:29', '2018-05-04 05:48:29'),
(27, 'aaaaaaa', 'YELI', 'Algeria', 'fvufjhvofv', '54545222222', 'ojpr15@gmail.com', 'gtgt', 'Provider', '2018-05-04 05:54:46', '2018-05-04 05:54:46'),
(38, '8697', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'jhvv', 'Client', '2018-05-04 08:22:16', '2018-05-04 08:22:16'),
(39, '87654324', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'www.cagua.com', 'Provider', '2018-05-04 23:21:45', '2018-05-04 23:21:45'),
(40, '5555555', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', '5tgtgt', 'Client', '2018-05-04 23:27:30', '2018-05-04 23:27:30'),
(41, '455444', 'JOSE PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'DDDDDDD', 'Client', '2018-05-04 23:32:48', '2018-05-04 23:32:48'),
(42, '65656', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'THTHT', 'Client', '2018-05-04 23:34:25', '2018-05-04 23:34:25'),
(43, '656569999', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'THTHT', 'Client', '2018-05-05 08:20:32', '2018-05-05 08:20:32'),
(45, '20989357', 'OSWARD PACHECO', 'Venezuela', 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '4243513322', 'ojpr15@gmail.com', 'WWW.GOOGLE.COM', 'Client', '2018-05-11 03:36:11', '2018-05-11 03:36:11'),
(46, '656569999', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'THTHT', 'Client', '2018-05-11 07:27:14', '2018-05-11 07:27:14'),
(47, '65656', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'THTHT', 'Client', '2018-05-11 07:27:34', '2018-05-11 07:27:34'),
(48, '65656', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'THTHT', 'Client', '2018-05-11 07:27:55', '2018-05-11 07:27:55'),
(49, '456554', 'tryryrt', 'Venezuela', 'rhthtrhht', '4243513322', 'oswardp_94@hotmail.com', 'thrth', 'Provider', '2018-05-11 07:41:29', '2018-05-11 07:41:29'),
(50, '454545', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'rfrff', 'Client', '2018-05-15 09:14:12', '2018-05-15 09:14:12'),
(51, '555', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'fffff', 'Provider', '2018-05-17 06:19:56', '2018-05-17 06:19:56'),
(52, '7678965', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'ojpr15@gmail.com', 'fgyhgghj', 'Client', '2018-05-17 22:29:02', '2018-05-17 22:29:02'),
(53, '435344', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'jacobo.ramirez@gmail.com', '4regerg', 'Client', '2018-05-17 22:34:40', '2018-05-17 22:34:40'),
(55, '56789', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'jacobo.ramirez@gmail.com', '3DEDED', 'Provider', '2018-05-26 04:50:03', '2018-05-26 04:50:03'),
(56, '5678', 'RAMIRO', 'Aland Islands', 'DKNDKN', '5354534', 'Osward@atomiclabco.com', 'RGRGEG', 'Client', '2018-05-26 06:34:57', '2018-05-26 06:34:57'),
(57, '20989357', 'José Pacheco', 'Venezuela', 'Cagua', '4243513322', 'ojpr15@gmail.com', 'jdjd', 'Client', '2018-05-26 08:24:41', '2018-05-26 08:24:41'),
(58, '20989357', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4444442', 'ojpr15@gmail.com', 'ss', 'Client', '2018-05-26 08:28:24', '2018-05-26 08:28:24'),
(59, '20989357', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4444442', 'ojpr15@gmail.com', 'ss', 'Client', '2018-05-26 08:28:37', '2018-05-26 08:28:37'),
(60, '11111111', 'Jose', 'Venezuela', 'cagua', '45566543', 'Osward@atomiclabco.com', 'gbrtg', 'Client', '2018-05-26 08:34:04', '2018-05-26 08:34:04'),
(61, '11111111', 'Jose', 'Venezuela', 'cagua', '45566543', 'Osward@atomiclabco.com', 'gbrtg', 'Client', '2018-05-26 08:34:07', '2018-05-26 08:34:07'),
(62, '434', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '553443443', 'ojpr15@gmail.com', 'rgergre', 'Client', '2018-05-26 08:35:19', '2018-05-26 08:35:19'),
(63, '45678', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '45678', 'oswardp_94@hotmail.com', 'ghjk', 'Provider', '2018-05-26 08:36:47', '2018-05-26 08:36:47'),
(64, '656', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '5636', 'JOSENRB@HOTMAIL.COM', 'reger', 'Provider', '2018-05-26 08:41:59', '2018-05-26 08:41:59'),
(65, '454545', 'RGRTGRG', 'Aland Islands', 'RGRGR', '5454545', 'ojpr15@gmail.com', 'RGGGR', 'Provider', '2018-05-26 08:43:37', '2018-05-26 08:43:37'),
(66, '8787870878', 'GRGRG', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '4243513322', 'oswardp_94@hotmail.com', 'GRGRG', 'Provider', '2018-05-26 08:44:13', '2018-05-26 08:44:13'),
(67, '12345678', 'ASD', 'Aland Islands', 'ASD', '3333331', 'DED@GMAIL.COM', 'DRFGT', 'Client', '2018-05-26 08:51:14', '2018-05-26 08:51:14'),
(68, '8584854', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '343334434', 'Osward@atomiclabco.com', 'RFRFR', 'Provider', '2018-05-26 08:53:05', '2018-05-26 08:53:05'),
(69, '456789', 'OSWARD PACHECO', 'Venezuela', 'Urb. Rafael Urdaneta\nSector #01 Calle #10 Casa #30', '45678', 'Osward@atomiclabco.com', 'ghjk', 'Provider', '2018-05-26 09:27:56', '2018-05-26 09:27:56'),
(70, '555555', 'Yelimar Gutiérrez', 'Venezuela', 'San Mateo', '04128668016', 'yeli@gmail.com', 'www.example.com', 'Provider', '2018-05-25 07:18:38', '2018-05-31 07:53:36'),
(73, '4567898765', 'Prueba', 'Venezuela', 'Cagua', '4243513322', 'ojpr15@gmail.com', 'www.on.com', 'Client', '2018-05-30 04:26:34', '2018-05-30 04:26:34'),
(72, '900373458', 'Moda SASssss', 'Colombia', 'Por ahi', '123121', 'modasas@live.com', 'modasas.com', 'Provider', '2018-05-30 00:24:56', '2018-05-31 12:15:17'),
(74, '4567898765', 'Prueba', 'Venezuela', 'Cagua, sector1', '4243513322', 'ojpr15@gmail.com', 'www.on.com', 'Client', '2018-05-30 04:27:37', '2018-05-30 04:27:37');

-- --------------------------------------------------------

--
-- Table structure for table `company_user`
--

CREATE TABLE `company_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_user`
--

INSERT INTO `company_user` (`id`, `company_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 54, 2, NULL, NULL),
(2, 55, 2, NULL, NULL),
(3, 67, 2, NULL, NULL),
(4, 68, 2, NULL, NULL),
(5, 69, 2, NULL, NULL),
(6, 70, 14, NULL, NULL),
(7, 71, 2, NULL, NULL),
(8, 72, 2, NULL, NULL),
(9, 73, 2, NULL, NULL),
(10, 74, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `first` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `first`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(6, 'Rosario Hernández', '04143259876', 'rosa@gmail.com', '2018-05-11 08:50:03', '2018-05-11 08:50:03'),
(3, 'Yeli', '0905345', 'yeli@gmail.com', '2018-05-03 21:57:50', '2018-05-03 21:57:50'),
(4, 'Pedro', '3675435435', 'pedro@gmail.com', '2018-05-04 00:13:15', '2018-05-04 00:13:15'),
(5, 'Hernán', '67847673', 'a@gmail.com', '2018-05-04 00:14:52', '2018-05-04 05:31:38'),
(7, 'JOSE', '4243513322', 'leonciorequena1995@gmail.com', '2018-05-26 06:38:34', '2018-05-26 06:38:34'),
(8, 'OSWARD', '4243513322', 'ojpr15@gmail.com', '2018-05-26 06:48:26', '2018-05-26 06:48:26'),
(9, 'AAAA', '4343434', 'leonciorequena1995@gmail.com', '2018-05-26 08:43:17', '2018-05-26 08:43:17'),
(10, 'UNO', '42234', 'ojpr15@gmail.com', '2018-05-26 08:51:37', '2018-05-26 08:51:37'),
(11, 'OSWARD', '4243513322', 'ojpr15@gmail.com', '2018-05-25 07:18:59', '2018-05-25 07:18:59'),
(12, 'leo', '4243513322', 'leonciorequena1995@gmail.com', '2018-05-25 07:19:14', '2018-05-25 07:19:14'),
(13, 'One', 'Two', 'Osward@atomiclabco.com', '2018-06-01 05:05:16', '2018-06-01 05:05:16');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `Codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Pais` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `Codigo`, `Pais`, `created_at`, `updated_at`) VALUES
(1, 'AU', 'Australia', NULL, NULL),
(2, 'CN', 'China', NULL, NULL),
(3, 'JP', 'Japan', NULL, '2018-05-18 05:15:15'),
(4, 'TH', 'Thailand', NULL, NULL),
(5, 'IN', 'India', NULL, NULL),
(6, 'MY', 'Malaysia', NULL, NULL),
(7, 'KR', 'Kore', NULL, NULL),
(8, 'HK', 'Hong Kong', NULL, NULL),
(9, 'TW', 'Taiwan', NULL, NULL),
(10, 'PH', 'Philippines', NULL, NULL),
(11, 'VN', 'Vietnam', NULL, NULL),
(12, 'FR', 'France', NULL, NULL),
(13, 'EU', 'Europe', NULL, NULL),
(14, 'DE', 'Germany', NULL, NULL),
(15, 'SE', 'Sweden', NULL, NULL),
(16, 'IT', 'Italy', NULL, NULL),
(17, 'GR', 'Greece', NULL, NULL),
(18, 'ES', 'Spain', NULL, NULL),
(19, 'AT', 'Austria', NULL, NULL),
(20, 'GB', 'United Kingdom', NULL, NULL),
(21, 'NL', 'Netherlands', NULL, NULL),
(22, 'BE', 'Belgium', NULL, NULL),
(23, 'CH', 'Switzerland', NULL, NULL),
(24, 'AE', 'United Arab Emirates', NULL, NULL),
(25, 'IL', 'Israel', NULL, NULL),
(26, 'UA', 'Ukraine', NULL, NULL),
(27, 'RU', 'Russian Federation', NULL, NULL),
(28, 'KZ', 'Kazakhstan', NULL, NULL),
(29, 'PT', 'Portugal', NULL, NULL),
(30, 'SA', 'Saudi Arabia', NULL, NULL),
(31, 'DK', 'Denmark', NULL, NULL),
(32, 'IR', 'Ira', NULL, NULL),
(33, 'NO', 'Norway', NULL, NULL),
(34, 'US', 'United States', NULL, NULL),
(35, 'MX', 'Mexico', NULL, NULL),
(36, 'CA', 'Canada', NULL, NULL),
(37, 'A1', 'Anonymous Proxy', NULL, NULL),
(38, 'SY', 'Syrian Arab Republic', NULL, NULL),
(39, 'CY', 'Cyprus', NULL, NULL),
(40, 'CZ', 'Czech Republic', NULL, NULL),
(41, 'IQ', 'Iraq', NULL, NULL),
(42, 'TR', 'Turkey', NULL, NULL),
(43, 'RO', 'Romania', NULL, NULL),
(44, 'LB', 'Lebanon', NULL, NULL),
(45, 'HU', 'Hungary', NULL, NULL),
(46, 'GE', 'Georgia', NULL, NULL),
(47, 'BR', 'Brazil', NULL, NULL),
(48, 'AZ', 'Azerbaijan', NULL, NULL),
(49, 'A2', 'Satellite Provider', NULL, NULL),
(50, 'PS', 'Palestinian Territory', NULL, NULL),
(51, 'LT', 'Lithuania', NULL, NULL),
(52, 'OM', 'Oman', NULL, NULL),
(53, 'SK', 'Slovakia', NULL, NULL),
(54, 'RS', 'Serbia', NULL, NULL),
(55, 'FI', 'Finland', NULL, NULL),
(56, 'IS', 'Iceland', NULL, NULL),
(57, 'BG', 'Bulgaria', NULL, NULL),
(58, 'SI', 'Slovenia', NULL, NULL),
(59, 'MD', 'Moldov', NULL, NULL),
(60, 'MK', 'Macedonia', NULL, NULL),
(61, 'LI', 'Liechtenstein', NULL, NULL),
(62, 'JE', 'Jersey', NULL, NULL),
(63, 'PL', 'Poland', NULL, NULL),
(64, 'HR', 'Croatia', NULL, NULL),
(65, 'BA', 'Bosnia and Herzegovina', NULL, NULL),
(66, 'EE', 'Estonia', NULL, NULL),
(67, 'LV', 'Latvia', NULL, NULL),
(68, 'JO', 'Jordan', NULL, NULL),
(69, 'KG', 'Kyrgyzstan', NULL, NULL),
(70, 'RE', 'Reunion', NULL, NULL),
(71, 'IE', 'Ireland', NULL, NULL),
(72, 'LY', 'Libya', NULL, NULL),
(73, 'LU', 'Luxembourg', NULL, NULL),
(74, 'AM', 'Armenia', NULL, NULL),
(75, 'VG', 'Virgin Island', NULL, NULL),
(76, 'YE', 'Yemen', NULL, NULL),
(77, 'BY', 'Belarus', NULL, NULL),
(78, 'GI', 'Gibraltar', NULL, NULL),
(79, 'MQ', 'Martinique', NULL, NULL),
(80, 'PA', 'Panama', NULL, NULL),
(81, 'DO', 'Dominican Republic', NULL, NULL),
(82, 'GU', 'Guam', NULL, NULL),
(83, 'PR', 'Puerto Rico', NULL, NULL),
(84, 'VI', 'Virgin Island', NULL, NULL),
(85, 'MN', 'Mongolia', NULL, NULL),
(86, 'NZ', 'New Zealand', NULL, NULL),
(87, 'SG', 'Singapore', NULL, NULL),
(88, 'ID', 'Indonesia', NULL, NULL),
(89, 'NP', 'Nepal', NULL, NULL),
(90, 'PG', 'Papua New Guinea', NULL, NULL),
(91, 'PK', 'Pakistan', NULL, NULL),
(92, 'AP', 'Asia/Pacific Region', NULL, NULL),
(93, 'BS', 'Bahamas', NULL, NULL),
(94, 'LC', 'Saint Lucia', NULL, NULL),
(95, 'AR', 'Argentina', NULL, NULL),
(96, 'BD', 'Bangladesh', NULL, NULL),
(97, 'TK', 'Tokelau', NULL, NULL),
(98, 'KH', 'Cambodia', NULL, NULL),
(99, 'MO', 'Macau', NULL, NULL),
(100, 'MV', 'Maldives', NULL, NULL),
(101, 'AF', 'Afghanistan', NULL, NULL),
(102, 'NC', 'New Caledonia', NULL, NULL),
(103, 'FJ', 'Fiji', NULL, NULL),
(104, 'WF', 'Wallis and Futuna', NULL, NULL),
(105, 'QA', 'Qatar', NULL, NULL),
(106, 'AL', 'Albania', NULL, NULL),
(107, 'BZ', 'Belize', NULL, NULL),
(108, 'UZ', 'Uzbekistan', NULL, NULL),
(109, 'KW', 'Kuwait', NULL, NULL),
(110, 'ME', 'Montenegro', NULL, NULL),
(111, 'PE', 'Peru', NULL, NULL),
(112, 'BM', 'Bermuda', NULL, NULL),
(113, 'CW', 'Curacao', NULL, NULL),
(114, 'CO', 'Colombia', NULL, NULL),
(115, 'VE', 'Venezuela', NULL, NULL),
(116, 'CL', 'Chile', NULL, NULL),
(117, 'EC', 'Ecuador', NULL, NULL),
(118, 'ZA', 'South Africa', NULL, NULL),
(119, 'IM', 'Isle of Man', NULL, NULL),
(120, 'BO', 'Bolivia', NULL, NULL),
(121, 'GG', 'Guernsey', NULL, NULL),
(122, 'MT', 'Malta', NULL, NULL),
(123, 'TJ', 'Tajikistan', NULL, NULL),
(124, 'SC', 'Seychelles', NULL, NULL),
(125, 'BH', 'Bahrain', NULL, NULL),
(126, 'EG', 'Egypt', NULL, NULL),
(127, 'ZW', 'Zimbabwe', NULL, NULL),
(128, 'LR', 'Liberia', NULL, NULL),
(129, 'KE', 'Kenya', NULL, NULL),
(130, 'GH', 'Ghana', NULL, NULL),
(131, 'NG', 'Nigeria', NULL, NULL),
(132, 'TZ', 'Tanzani', NULL, NULL),
(133, 'ZM', 'Zambia', NULL, NULL),
(134, 'MG', 'Madagascar', NULL, NULL),
(135, 'AO', 'Angola', NULL, NULL),
(136, 'NA', 'Namibia', NULL, NULL),
(137, 'CI', 'Cote D\'Ivoire', NULL, NULL),
(138, 'SD', 'Sudan', NULL, NULL),
(139, 'CM', 'Cameroon', NULL, NULL),
(140, 'MW', 'Malawi', NULL, NULL),
(141, 'GA', 'Gabon', NULL, NULL),
(142, 'ML', 'Mali', NULL, NULL),
(143, 'BJ', 'Benin', NULL, NULL),
(144, 'TD', 'Chad', NULL, NULL),
(145, 'BW', 'Botswana', NULL, NULL),
(146, 'CV', 'Cape Verde', NULL, NULL),
(147, 'RW', 'Rwanda', NULL, NULL),
(148, 'CG', 'Congo', NULL, NULL),
(149, 'UG', 'Uganda', NULL, NULL),
(150, 'MZ', 'Mozambique', NULL, NULL),
(151, 'GM', 'Gambia', NULL, NULL),
(152, 'LS', 'Lesotho', NULL, NULL),
(153, 'MU', 'Mauritius', NULL, NULL),
(154, 'MA', 'Morocco', NULL, NULL),
(155, 'DZ', 'Algeria', NULL, NULL),
(156, 'GN', 'Guinea', NULL, NULL),
(157, 'CD', 'Cong', NULL, NULL),
(158, 'SZ', 'Swaziland', NULL, NULL),
(159, 'BF', 'Burkina Faso', NULL, NULL),
(160, 'SL', 'Sierra Leone', NULL, NULL),
(161, 'SO', 'Somalia', NULL, NULL),
(162, 'NE', 'Niger', NULL, NULL),
(163, 'CF', 'Central African Republic', NULL, NULL),
(164, 'TG', 'Togo', NULL, NULL),
(165, 'BI', 'Burundi', NULL, NULL),
(166, 'GQ', 'Equatorial Guinea', NULL, NULL),
(167, 'SS', 'South Sudan', NULL, NULL),
(168, 'SN', 'Senegal', NULL, NULL),
(169, 'MR', 'Mauritania', NULL, NULL),
(170, 'DJ', 'Djibouti', NULL, NULL),
(171, 'KM', 'Comoros', NULL, NULL),
(172, 'IO', 'British Indian Ocean Territory', NULL, NULL),
(173, 'TN', 'Tunisia', NULL, NULL),
(174, 'GL', 'Greenland', NULL, NULL),
(175, 'VA', 'Holy See (Vatican City State)', NULL, NULL),
(176, 'CR', 'Costa Rica', NULL, NULL),
(177, 'KY', 'Cayman Islands', NULL, NULL),
(178, 'JM', 'Jamaica', NULL, NULL),
(179, 'GT', 'Guatemala', NULL, NULL),
(180, 'MH', 'Marshall Islands', NULL, NULL),
(181, 'AQ', 'Antarctica', NULL, NULL),
(182, 'BB', 'Barbados', NULL, NULL),
(183, 'AW', 'Aruba', NULL, NULL),
(184, 'MC', 'Monaco', NULL, NULL),
(185, 'AI', 'Anguilla', NULL, NULL),
(186, 'KN', 'Saint Kitts and Nevis', NULL, NULL),
(187, 'GD', 'Grenada', NULL, NULL),
(188, 'PY', 'Paraguay', NULL, NULL),
(189, 'MS', 'Montserrat', NULL, NULL),
(190, 'TC', 'Turks and Caicos Islands', NULL, NULL),
(191, 'AG', 'Antigua and Barbuda', NULL, NULL),
(192, 'TV', 'Tuvalu', NULL, NULL),
(193, 'PF', 'French Polynesia', NULL, NULL),
(194, 'SB', 'Solomon Islands', NULL, NULL),
(195, 'VU', 'Vanuatu', NULL, NULL),
(196, 'ER', 'Eritrea', NULL, NULL),
(197, 'TT', 'Trinidad and Tobago', NULL, NULL),
(198, 'AD', 'Andorra', NULL, NULL),
(199, 'HT', 'Haiti', NULL, NULL),
(200, 'SH', 'Saint Helena', NULL, NULL),
(201, 'FM', 'Micronesi', NULL, NULL),
(202, 'SV', 'El Salvador', NULL, NULL),
(203, 'HN', 'Honduras', NULL, NULL),
(204, 'UY', 'Uruguay', NULL, NULL),
(205, 'LK', 'Sri Lanka', NULL, NULL),
(206, 'EH', 'Western Sahara', NULL, NULL),
(207, 'CX', 'Christmas Island', NULL, NULL),
(208, 'WS', 'Samoa', NULL, NULL),
(209, 'SR', 'Suriname', NULL, NULL),
(210, 'CK', 'Cook Islands', NULL, NULL),
(211, 'KI', 'Kiribati', NULL, NULL),
(212, 'NU', 'Niue', NULL, NULL),
(213, 'TO', 'Tonga', NULL, NULL),
(214, 'TF', 'French Southern Territories', NULL, NULL),
(215, 'YT', 'Mayotte', NULL, NULL),
(216, 'NF', 'Norfolk Island', NULL, NULL),
(217, 'BN', 'Brunei Darussalam', NULL, NULL),
(218, 'TM', 'Turkmenistan', NULL, NULL),
(219, 'PN', 'Pitcairn Islands', NULL, NULL),
(220, 'SM', 'San Marino', NULL, NULL),
(221, 'AX', 'Aland Islands', NULL, NULL),
(222, 'FO', 'Faroe Islands', NULL, NULL),
(223, 'SJ', 'Svalbard and Jan Mayen', NULL, NULL),
(224, 'CC', 'Cocos (Keeling) Islands', NULL, NULL),
(225, 'NR', 'Nauru', NULL, NULL),
(226, 'GS', 'South Georgia and the South Sandwich Islands', NULL, NULL),
(227, 'UM', 'United States Minor Outlying Islands', NULL, NULL),
(228, 'GW', 'Guinea-Bissau', NULL, NULL),
(229, 'PW', 'Palau', NULL, NULL),
(230, 'AS', 'American Samoa', NULL, NULL),
(231, 'BT', 'Bhutan', NULL, NULL),
(232, 'GF', 'French Guiana', NULL, NULL),
(233, 'GP', 'Guadeloupe', NULL, NULL),
(234, 'MF', 'Saint Martin', NULL, NULL),
(235, 'VC', 'Saint Vincent and the Grenadines', NULL, NULL),
(236, 'PM', 'Saint Pierre and Miquelon', NULL, NULL),
(237, 'BL', 'Saint Barthelemy', NULL, NULL),
(238, 'DM', 'Dominica', NULL, NULL),
(239, 'ST', 'Sao Tome and Principe', NULL, NULL),
(240, 'KP', 'Kore', NULL, NULL),
(241, 'FK', 'Falkland Islands (Malvinas)', NULL, NULL),
(242, 'MP', 'Northern Mariana Islands', NULL, NULL),
(243, 'TL', 'Timor-Leste', NULL, NULL),
(244, 'BQ', 'Bonair', NULL, NULL),
(245, 'MM', 'Myanmar', NULL, NULL),
(246, 'NI', 'Nicaragua', NULL, NULL),
(247, 'SX', 'Sint Maarten (Dutch part)', NULL, NULL),
(248, 'GY', 'Guyana', NULL, NULL),
(249, 'LA', 'Lao People\'s Democratic Republic', NULL, NULL),
(250, 'CU', 'Cuba', NULL, NULL),
(251, 'ET', 'Ethiopia', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `ab_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ab_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ab_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ab_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ab_message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `ab_name`, `ab_email`, `ab_phone`, `ab_date`, `ab_message`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'OSWARD PACHECO', 'ojpr15@gmail.com', '4243513322', '2018-05-25', '<h2>Hola, soy José Pacheco, saludos.</h2><p>Gerencia.<br></p>', 14, '2018-05-25 08:43:40', '2018-05-25 08:43:40'),
(2, 'PRUEBA 2', 'ojpr15@gmail.com', '4243513322', '2018-05-25', '<p>EDEDEDED</p>', 2, '2018-05-25 08:44:35', '2018-05-25 08:44:35'),
(3, 'rrr', 'ojpr15@gmail.com', '4243513322', '2018-05-25', '<p>rrrr</p>', 14, '2018-05-25 09:46:36', '2018-05-25 09:46:36'),
(4, 'OSWARD PACHECO', 'ojpr15@gmail.com', '4243513322', '2018-05-25', '<p>frfrfr</p>', 14, '2018-05-25 09:48:49', '2018-05-25 09:48:49'),
(5, 'OSWARD PACHECO', 'ojpr15@gmail.com', '4243513322', '2018-05-25', '<p>dwedde</p>', 14, '2018-05-25 09:50:16', '2018-05-25 09:50:16'),
(6, 'OSWARD PACHECO', 'ojpr15@gmail.com', '4243513322', '2018-05-25', '<p>dcdcc</p>', 14, '2018-05-25 09:51:11', '2018-05-25 09:51:11'),
(7, 'Prueba', 'desarrollodigitalarmenia@gmail.com', '31321321', '2018-05-30', '<p>Prueba zdfgsdfg <u>dfgdfgdfg&nbsp; <i></i></u><small>dfg</small><u><i></i></u><b></b></p>', 2, '2018-05-30 02:35:40', '2018-05-30 02:35:40');

-- --------------------------------------------------------

--
-- Table structure for table `global`
--

CREATE TABLE `global` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `contact_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `global`
--

INSERT INTO `global` (`id`, `company_id`, `contact_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, NULL, NULL),
(2, 1, 1, NULL, NULL),
(3, 3, 2, NULL, NULL),
(4, 3, 1, NULL, NULL),
(5, 4, 3, NULL, NULL),
(6, 4, 2, NULL, NULL),
(7, 5, 3, NULL, NULL),
(8, 5, 1, NULL, NULL),
(9, 6, 3, NULL, NULL),
(10, 6, 1, NULL, NULL),
(11, 7, 3, NULL, NULL),
(12, 7, 1, NULL, NULL),
(13, 8, 2, NULL, NULL),
(14, 8, 1, NULL, NULL),
(15, 36, 5, NULL, NULL),
(16, 36, 4, NULL, NULL),
(17, 38, 4, NULL, NULL),
(18, 38, 3, NULL, NULL),
(19, 39, 5, NULL, NULL),
(20, 39, 3, NULL, NULL),
(21, 40, 4, NULL, NULL),
(22, 44, 4, NULL, NULL),
(23, 45, 5, NULL, NULL),
(24, 45, 4, NULL, NULL),
(25, 45, 3, NULL, NULL),
(26, 46, 5, NULL, NULL),
(27, 46, 4, NULL, NULL),
(28, 47, 5, NULL, NULL),
(29, 47, 4, NULL, NULL),
(30, 48, 5, NULL, NULL),
(31, 49, 4, NULL, NULL),
(32, 50, 4, NULL, NULL),
(33, 51, 5, NULL, NULL),
(34, 52, 4, NULL, NULL),
(35, 53, 3, NULL, NULL),
(36, 56, 8, NULL, NULL),
(37, 64, 9, NULL, NULL),
(38, 67, 10, NULL, NULL),
(39, 70, 11, NULL, NULL),
(40, 70, 12, NULL, NULL),
(41, 70, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `invoice_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_issue` date NOT NULL,
  `date_expiration` date NOT NULL,
  `rode` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `company_id`, `state_id`, `invoice_number`, `date_issue`, `date_expiration`, `rode`, `file`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '4545435', '2018-05-19', '2018-05-25', '4553.00', '', '2018-05-12 02:17:05', '2018-05-12 02:17:05'),
(2, 2, 2, '2343434', '2018-05-11', '2018-05-22', '4423.00', '', '2018-05-12 02:18:36', '2018-05-12 02:18:36'),
(3, 4, 1, '54676456', '2018-05-15', '2018-05-24', '45454.00', '', '2018-05-15 18:34:54', '2018-05-15 18:34:54'),
(4, 2, 1, '567896543234567', '2018-05-15', '2018-05-23', '345678.00', '', '2018-05-15 18:43:39', '2018-05-15 18:43:39'),
(5, 4, 1, '6545456456', '2018-05-15', '2018-05-21', '789675.00', '', '2018-05-15 18:45:05', '2018-05-15 18:45:05'),
(6, 1, 1, '54545423', '2018-05-15', '2018-05-26', '454254.00', '', '2018-05-15 19:58:22', '2018-05-15 19:58:22'),
(7, 7, 2, '44254524', '2018-05-15', '2018-05-24', '42545.00', '', '2018-05-15 20:09:32', '2018-05-15 20:09:32'),
(12, 2, 9, '45678', '2018-05-26', '2018-05-24', '45678.00', 'http://localhost/AppWork-M/public/invoices/JhEGN34eBulkIsGohVPmnFFWDZgPvn8bnuuyTRIC.pdf', '2018-05-26 09:38:17', '2018-05-26 09:38:18'),
(9, 3, 10, '4544', '2018-05-17', '2018-05-19', '54545.00', '', '2018-05-17 22:49:51', '2018-05-17 22:49:51'),
(10, 2, 9, '534', '2018-05-18', '2018-05-19', '543543.00', 'apple_developer_agreement.pdf', '2018-05-19 00:47:48', '2018-05-19 00:47:48'),
(11, 2, 10, '787878', '2018-05-18', '2018-05-23', '63333.00', 'ejercicios_5.pdf', '2018-05-19 00:48:22', '2018-05-19 00:48:22'),
(13, 3, 9, '999999999999', '2018-05-26', '2018-05-24', '6789.00', 'http://localhost/AppWork-M/public/invoices/CjNHf9Pi09VFfJnPRqdK6dVAW30jO0bMZZ0UjTyI.png', '2018-05-26 09:38:54', '2018-05-26 09:38:54'),
(14, 74, 10, '0002', '2018-06-01', '2018-06-05', '1500.00', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/img/diploma-curso-php-laravel.pdf', '2018-05-30 02:26:23', '2018-06-01 04:18:25'),
(15, 1, 9, '5678', '2018-05-30', '2018-05-31', '45678.00', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/invoices/crZFCBf8Xf9UkE05Ya41KeQEB4V1K2MTCmrYdASm.pdf', '2018-05-30 04:29:02', '2018-05-30 04:29:02'),
(16, 72, 9, '000252', '2018-06-01', '2018-06-07', '125.00', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/invoices/Z0TL1Iv9RJKAnXlxLUbrwXTEZlIXjQP6YhJXxNrN.jpeg', '2018-05-31 12:17:04', '2018-05-31 12:17:04'),
(17, 39, 9, '345435345', '2018-05-31', '2018-06-02', '20000.00', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/invoices/HwIfajUrye5VzO2qQ9dB93GZUNHRG2WhoBeuYQ3f.pdf', '2018-05-31 13:17:01', '2018-05-31 13:17:01'),
(18, 1, 10, '535', '2018-05-31', '2018-05-31', '2000.00', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/invoices/itqcdDwg9Q6jZ9pFoRnH3pNSG4rQs7GmWIQA8ogx.png', '2018-05-31 13:24:30', '2018-05-31 13:24:31'),
(19, 2, 9, '4534535', '2018-05-31', '2018-05-31', '453534.00', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/img/Koala.jpg', '2018-05-31 20:58:02', '2018-05-31 20:58:02'),
(20, 74, 10, '23333', '2018-05-31', '2018-06-02', '444444444', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/img/Tulips.jpg', '2018-05-31 21:03:43', '2018-05-31 21:10:30'),
(21, 6, 9, '3334444', '2018-05-31', '2018-06-03', '44443333', 'http://prueba2.efiempresa.com/admin/AppWork-master/public/img/Chrysanthemum.jpg', '2018-05-31 21:18:23', '2018-05-31 21:18:23'),
(22, 6, 9, '567', '2018-05-31', '2018-05-31', '345678', 'http://prueba2.efiempresa.com/admin/AppWork-master/public/imagen/Jellyfish.jpg', '2018-05-31 21:29:24', '2018-05-31 21:29:24'),
(23, 1, 10, '7777', '2018-05-31', '2018-06-14', '77777', 'http://prueba2.efiempresa.com/admin/AppWork-master/public/img/Chrysanthemum.jpg', '2018-05-31 21:43:02', '2018-05-31 21:43:02'),
(24, 6, 9, '44444', '2018-05-31', '2018-06-07', '44444', 'http://prueba2.efiempresa.com/admin/AppWork-master/public/img/Lighthouse.jpg', '2018-05-31 21:48:04', '2018-05-31 21:48:04'),
(25, 74, 10, '11122', '2018-06-01', '2018-05-16', '11122', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/img/Hydrangeas.jpg', '2018-06-01 04:06:39', '2018-06-01 04:15:46'),
(26, 6, 9, '3333', '2018-06-01', '2018-05-16', '33333', 'https://prueba2.efiempresa.com/admin/AppWork-master/public/img/apple_developer_agreement.pdf', '2018-06-01 04:17:44', '2018-06-01 04:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_26_053843_create_states_table', 1),
(4, '2018_04_30_030736_create_contacts_table', 1),
(5, '2018_04_30_031354_create_companies_table', 1),
(6, '2018_04_30_034032_create_global_table', 1),
(7, '2018_04_30_034604_create_company_user_table', 1),
(8, '2018_04_30_035111_create_tasks_table', 1),
(9, '2018_04_30_035332_create_invoices_table', 1),
(10, '2018_05_18_000900_create_countries_table', 2),
(11, '2018_05_23_045941_create_mail_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `nombre`) VALUES
(1, 'States'),
(2, 'Countrys'),
(3, 'Companys'),
(4, 'Invoices'),
(5, 'Tasks'),
(6, 'Users'),
(7, 'Reports');

-- --------------------------------------------------------

--
-- Table structure for table `mod_usu`
--

CREATE TABLE `mod_usu` (
  `id` int(11) NOT NULL,
  `id_mod` int(11) NOT NULL,
  `id_usu` int(11) NOT NULL,
  `valor` enum('true','false') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mod_usu`
--

INSERT INTO `mod_usu` (`id`, `id_mod`, `id_usu`, `valor`) VALUES
(63, 7, 8, 'true'),
(62, 6, 8, 'true'),
(61, 5, 8, 'true'),
(60, 4, 8, 'true'),
(59, 3, 8, 'true'),
(58, 2, 8, 'true'),
(57, 1, 8, 'true'),
(142, 7, 2, 'true'),
(141, 6, 2, 'true'),
(140, 5, 2, 'true'),
(139, 4, 2, 'true'),
(138, 3, 2, 'true'),
(137, 2, 2, 'true'),
(136, 1, 2, 'true'),
(64, 3, 5, 'true'),
(65, 5, 5, 'true'),
(125, 3, 12, 'true'),
(126, 4, 12, 'true'),
(148, 4, 14, 'true'),
(129, 5, 13, 'true'),
(147, 3, 14, 'true'),
(149, 5, 14, 'true'),
(150, 1, 15, 'true'),
(151, 2, 15, 'true'),
(152, 3, 15, 'true'),
(153, 4, 15, 'true'),
(154, 5, 15, 'true'),
(155, 6, 15, 'true'),
(156, 7, 15, 'true'),
(166, 6, 16, 'true'),
(165, 4, 16, 'true'),
(164, 2, 16, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ojpr15@gmail.com', '$2y$10$qG.XN9m1IOTp7FyzeyBCTOAvW1mSsAwC7yACfKelzlgGotxJ58a5e', '2018-05-31 07:46:31'),
('leonciorequena1995@gmail.com', '$2y$10$rhC8mO95ViB/YYY5iovsf.9Sa5XoRHlimmvdaQkllrrS/MjjWUzq6', '2018-05-23 07:40:25'),
('ojpr15@yahoo.com', '$2y$10$en04SjMAYJ9LRsRN2rtzRO/Uk6bbvsQV7Jg6nNPDj61JYJP1Lmv5e', '2018-05-23 07:05:02');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `name2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Task','Invoice') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name2`, `type`, `created_at`, `updated_at`) VALUES
(1, 'ACTIVO', 'Task', '2018-05-04 00:16:00', '2018-05-04 00:16:00'),
(3, 'EN PROGRESO', 'Task', '2018-05-05 08:21:07', '2018-05-17 09:32:18'),
(9, 'VENCIDA', 'Invoice', '2018-05-17 22:47:08', '2018-05-17 22:47:08'),
(5, 'CANCELLED', 'Task', '2018-05-17 09:10:48', '2018-05-17 09:10:48'),
(10, 'NO VENCIDA', 'Invoice', '2018-05-17 22:47:16', '2018-05-17 22:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `date_task` date NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `company_id`, `user_id`, `state_id`, `date_task`, `description`, `created_at`, `updated_at`) VALUES
(1, 49, 2, 1, '2018-05-18', 'prueba', '2018-05-11 09:17:33', '2018-05-11 09:17:33'),
(2, 47, 3, 3, '2018-05-17', 'dfgtrhg', '2018-05-11 23:05:50', '2018-05-11 23:05:50'),
(3, 45, 2, 2, '2018-05-25', 'nhjjhjh', '2018-05-11 23:08:39', '2018-05-11 23:08:39'),
(4, 49, 3, 1, '2018-05-24', 'ejehhelh', '2018-05-11 23:29:12', '2018-05-11 23:29:12'),
(5, 46, 3, 1, '2018-05-30', 'efeef', '2018-05-11 23:35:33', '2018-05-12 01:04:44'),
(7, 49, 4, 3, '2018-05-23', 'fwefre', '2018-05-12 01:11:32', '2018-05-12 01:11:32'),
(8, 47, 3, 1, '2018-05-23', 'dkjdjnelnclnwe', '2018-05-15 08:08:33', '2018-05-15 08:08:33'),
(9, 48, 2, 1, '2018-05-14', 'rrefrf', '2018-05-15 08:09:47', '2018-05-15 08:09:47'),
(10, 48, 2, 1, '2018-05-15', 'ferfrefref', '2018-05-15 08:59:34', '2018-05-15 08:59:34'),
(11, 46, 3, 3, '2018-05-15', 'ergtegte', '2018-05-15 09:06:48', '2018-05-15 09:06:48'),
(12, 49, 3, 1, '2018-05-15', 'dd4d4', '2018-05-15 09:12:58', '2018-05-15 09:12:58'),
(13, 50, 3, 1, '2018-05-15', 'fefef', '2018-05-15 09:14:38', '2018-05-15 09:14:38'),
(14, 52, 3, 1, '2018-05-19', 'frfrf', '2018-05-17 22:51:26', '2018-05-17 22:51:26'),
(15, 51, 7, 1, '2018-05-24', 'HACER ESTO', '2018-05-24 03:24:41', '2018-05-24 03:24:41'),
(16, 49, 12, 1, '2018-05-28', 'gg', '2018-05-26 03:48:10', '2018-05-26 03:48:10'),
(17, 1, 14, 1, '2018-06-01', 'ACTUALIZADO 31/05', '2018-05-25 08:30:10', '2018-06-01 05:22:35'),
(18, 70, 2, 1, '2018-05-25', 'rgrgtgtg', '2018-05-25 09:17:42', '2018-05-25 09:17:42'),
(19, 69, 2, 1, '2018-05-30', 'fgdbf', '2018-05-27 17:06:03', '2018-05-27 17:06:03'),
(20, 72, 2, 1, '2018-06-05', 'sdfsdf', '2018-05-30 02:30:20', '2018-05-30 02:30:20'),
(21, 70, 2, 1, '2018-05-31', 'CAGUA', '2018-05-30 19:30:02', '2018-05-30 19:30:02'),
(22, 72, 15, 1, '2018-05-31', 'fvrv', '2018-05-30 19:35:04', '2018-05-30 19:35:04'),
(23, 74, 14, 5, '2018-05-31', 'xsdfgsdfgsdfg', '2018-05-31 12:18:47', '2018-05-31 12:18:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` enum('Admin','Seller') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `rol`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Osward José', 'ojpr15@gmail.com', '$2y$10$wYOnwzOBHozxxBY/iCOK3en7uRa/GmVKwZyiCfEUdKJRPIEOeBOuC', 'Admin', '5MXlzX9nms3xR5TQAEnedXgTpr8OZvziWhA9e86hefVFFu3nfqFEitNHVxUW', '2018-05-03 22:04:29', '2018-05-24 03:22:48'),
(14, 'José', 'oswardp_94@hotmail.com', '$2y$10$TqDe6ba5wLik2V4ri.QcjO7rd18dgFCi0hE0nI0.Nw2hHgXiqCZtG', 'Seller', '3OOe4L2PXGlrvIyIANPAJ9oKQwEWJLvk0e5wXA77xjrXcg9WKMF0qNl8sm35', '2018-05-26 07:17:13', '2018-05-26 07:17:49'),
(15, 'Desarrollo Digital', 'desarrollodigitalarmenia@gmail.com', '$2y$10$NyHW0gx6w1MpKly4M5T8Vu967W/KL8jggioBsV2gWslwxN1S2cL6.', 'Seller', NULL, '2018-05-30 00:11:00', '2018-05-30 00:11:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_user`
--
ALTER TABLE `company_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_user_company_id_foreign` (`company_id`),
  ADD KEY `company_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `global`
--
ALTER TABLE `global`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_company_id_foreign` (`company_id`),
  ADD KEY `global_contact_id_foreign` (`contact_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_company_id_foreign` (`company_id`),
  ADD KEY `invoices_state_id_foreign` (`state_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mod_usu`
--
ALTER TABLE `mod_usu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_company_id_foreign` (`company_id`),
  ADD KEY `tasks_user_id_foreign` (`user_id`),
  ADD KEY `tasks_state_id_foreign` (`state_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `company_user`
--
ALTER TABLE `company_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `global`
--
ALTER TABLE `global`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mod_usu`
--
ALTER TABLE `mod_usu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
