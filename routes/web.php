<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function () {
    return view('welcome');
});

Route::resource('contacts','ContactsController');
Route::resource('states','StatesController');
Route::resource('users','UserController');
Route::resource('companies','CompanyController');
Route::resource('invoice','InvoiceController');
Route::resource('tasks','TaskController');
Route::resource('emails','EmailController');
Route::resource('countries','CountryController');
Route::resource('reports','ReportController');
Route::resource('invoice','InvoiceController');
Route::get('invoiceBorrar/{id}/','InvoiceController@invoiceBorrar')->name('invoiceBorrar');

Route::get('/emails/create', 'EmailController@create');

Route::get('/home', 'HomeController@index');

Route::get('/email', 'TaskController@email');

Route::get('/listusers', 'UserController@index');

Route::get('/security', 'UserController@seguridad');

Route::put('/cambio_clave', 'UserController@cambio_clave');

Route::get('/usuarios/show', 'UserController@show');

Route::get('/reports/companies', 'ReportController@index');

Route::get('/reports/clients', 'ReportController@create');

Route::get('pdfcompany',array('as'=>'pdfcompany','uses'=>'ReportController@uno'));

Route::get('pdfclient',array('as'=>'pdfclient','uses'=>'ReportController@dos'));